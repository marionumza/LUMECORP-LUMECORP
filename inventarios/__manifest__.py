# -*- coding: utf-8 -*-

{
    'name': "Inventarios",
    'summary': """
        Ajustes y apoyo para el módulo de stock
    """,
    'description': """
        Módulo que permite generar reportes de inventarios valorados y ajustes para el módulo de stock.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'stock',
    ],
    'data': [
        'wizard/views/stock_inventario_wiz_view.xml',
    ],
    'application': False,
}