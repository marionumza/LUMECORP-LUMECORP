# -*- coding: utf-8 -*-

import time
from time import mktime
import base64
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from io import StringIO
from odoo import api, fields, models
import io
try:
    import xlwt
except ImportError:
    xlwt = None
import logging
_logger = logging.getLogger(__name__)

class StockInventarioWiz(models.TransientModel):
    _name = 'stock.inventario.wiz'
    _description = 'Reporte de Stock de Inventarios'

    fecha_inicial = fields.Date(string='Desde la Fecha', required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    fecha_final = fields.Date(string='Hasta la Fecha', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    tipo_lugar = fields.Selection([('Almacén','Almacén'),('Ubicación','Ubicación')], string='Tipo Lugar', default='Ubicación')
    ubicaciones = fields.Many2many('stock.location', string='Ubicaciones', domain="[('usage','=','internal'), ('active','=',True)]")
    almacenes = fields.Many2many('stock.warehouse', string='Almacenes')
    productos = fields.Many2many('product.product', string='Productos', domain="[('type','=','product')]")

    def imprimir_excel(self):
        filename = 'Stock Inventario.xls'
        workbook = xlwt.Workbook(encoding="UTF-8")
        worksheet = workbook.add_sheet('Kardex de Productos')
        worksheet.set_portrait(True)  # Posicion del Papel Vertical
        worksheet.paper_size_code = 1  # Tamaño del Papel: Carta

        stylePC = xlwt.XFStyle()
        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_CENTER
        fontP = xlwt.Font()
        fontP.bold = True
        fontP.height = 200
        stylePC.font = fontP
        stylePC.num_format_str = '@'
        stylePC.alignment = alignment

        worksheet.col(0).width = 260 * 18
        worksheet.col(1).width = 260 * 8
        worksheet.col(2).width = 260 * 22
        worksheet.col(3).width = 260 * 7
        worksheet.col(4).width = 260 * 7
        worksheet.col(5).width = 260 * 7
        worksheet.col(6).width = 260 * 7
        worksheet.col(7).width = 260 * 7
        worksheet.col(8).width = 260 * 7
        worksheet.col(9).width = 260 * 8

        titulo = xlwt.easyxf('font:height 400, bold True, name Arial Narrow; align: horiz center, vert center;')
        titulo2 = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz center, vert center,wrap on;borders: top medium,bottom medium')
        titulo3 = xlwt.easyxf('font:height 200, bold True, name Arial Narrow; align: horiz center, vert center')
        titulo4 = xlwt.easyxf('font:height 200, bold True, name Arial Narrow; align: horiz left, vert center')
        titulo5 = xlwt.easyxf('font:height 280, bold True, name Arial Narrow; align: horiz left, vert center')
        info = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz left, vert center;')
        info2 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz left, vert center;')

        datos1 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz left, vert center, wrap on;')
        datos2 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz center, vert center, wrap on;')
        datos3 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz right, vert center, wrap on;',num_format_str='#,##0.00')
        datos3_negrita = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz right, vert center, wrap on;',num_format_str='#,##0.00')

        totales = xlwt.easyxf('font:height 140, bold True; align: horiz right, vert center; borders: top medium',num_format_str='#,##0.00')

        worksheet.write_merge(0,1,0,9, 'KARDEX DE PRODUCTOS', titulo)
        fecha_impreso = 'Fecha Impreso:  ' + str((datetime.now() - timedelta(hours = 4)).strftime('%d-%m-%Y %H:%M:%S'))
        worksheet.write_merge(2,2,0,2,fecha_impreso, info)
        fecha_inicial = 'Fecha inicial: ' + str(self.fecha_inicial)
        worksheet.write_merge(2,2,3,5,fecha_inicial, info)
        fecha_final = 'Fecha final: ' + str(self.fecha_final)
        worksheet.write_merge(2,2,6,9,fecha_final, info)

        worksheet.write_merge(3,4,0,0,'Comprobante',titulo2)
        worksheet.write_merge(3,4,1,1,'Fecha',titulo2)
        worksheet.write_merge(3,4,2,2,'Cliente/Proveedor',titulo2)
        worksheet.write_merge(3,3,3,5,'Inventario Físico',titulo2)
        worksheet.write(4,3,'Entrada',titulo2)
        worksheet.write(4,4,'Salida',titulo2)
        worksheet.write(4,5,'Saldo',titulo2)

        filas = 4

        products = self.env['product.product'].search([],order='name asc')
        tipo_lugar = ''
        if self.productos: products = self.env['product.product'].search([('id','in',self.productos.ids)],order='name asc')
        if self.tipo_lugar == 'Ubicación':
            lugar = self.env['stock.location'].search([('usage','=','internal'), ('active','=',True)], order='name asc')
            if self.ubicaciones: lugar = self.env['stock.location'].search([('id','in',self.ubicaciones.ids)],order='name asc')
        elif self.tipo_lugar == 'Almacén':
            lugar = self.env['stock.warehouse'].search([], order='name asc')
            if self.almacenes: lugar = self.env['stock.warehouse'].search([('id','in',self.almacenes.ids)],order='name asc')

        for l in lugar:
            filas += 1
            producto = ''
            worksheet.write_merge(filas,filas,0,9,('Ubicacion - ' + str(str(l.location_id.name)+'/'+str(l.name))), titulo5)
            for p in products:
                if p.product_tmpl_id.type == 'product':
                    if producto != p.name_get()[0][1]:
                        filas += 1
                        producto = str(p.name_get()[0][1])
                        worksheet.write_merge(filas,filas,0,2,producto, titulo4)
                        if self.tipo_lugar == 'Ubicación': tipo_lugar = 'location'
                        elif self.tipo_lugar == 'Almacén': tipo_lugar = 'warehouse'
                        saldo_incial = p.with_context({tipo_lugar: l.id})._compute_quantities_dict(False, False, False, False, fields.Datetime.to_datetime(self.fecha_inicial) - timedelta(minutes=1))[p.id]['qty_available']
                        if saldo_incial != 0:
                            filas += 1
                            worksheet.write_merge(filas, filas, 0, 4, 'Saldo Inicial', info)  # Entrada
                            worksheet.write(filas,5,saldo_incial,datos3_negrita)  # Saldo
                    self.env.cr.execute("\
                        SELECT\
                        sm.id\
                        FROM stock_move sm\
                        WHERE (sm.location_id = "+str(l.id)+"\
                        OR sm.location_dest_id = "+str(l.id)+")\
                        AND sm.product_id = "+str(p.id)+"\
                        AND sm.state = 'done'\
                        AND ((sm.date AT TIME ZONE 'UTC' AT TIME ZONE 'BOT')::timestamp::date) BETWEEN '"+str(self.fecha_inicial)+"' AND '"+str(self.fecha_final)+"'\
                        ORDER BY sm.date\
                    ")
                    resultado_consulta = [i[0] for i in self.env.cr.fetchall()]
                    if saldo_incial == 0 and len(resultado_consulta) == 0:
                        filas += 1
                        worksheet.write(filas, 5, 0, datos3_negrita)  # Saldo
                    for move in resultado_consulta:
                        movimiento = self.env['stock.move'].search([('id','=',move)])
                        documento = ''
                        entrada = 0
                        salida = 0

                        if l.id == movimiento.location_id.id:
                            documento = movimiento.reference or movimiento.inventory_id.name
                            entrada = 0
                            salida = movimiento.product_uom_qty

                        if l.id == movimiento.location_dest_id.id:
                            documento = movimiento.reference or movimiento.inventory_id.name
                            entrada = movimiento.product_uom_qty
                            salida = 0

                        saldo_incial = saldo_incial + entrada - salida

                        struct_time_convert = time.strptime(str(movimiento.date), '%Y-%m-%d %H:%M:%S')
                        date_time_convert = datetime.fromtimestamp(mktime(struct_time_convert))
                        date_time_convert = date_time_convert - timedelta(hours=4)
                        fecha_movimiento = date_time_convert.strftime('%d/%m/%Y')

                        filas += 1
                        worksheet.write(filas,0,documento, datos1)  # Documento
                        worksheet.write(filas,1,fecha_movimiento, datos2)  # Fecha
                        worksheet.write(filas,2,movimiento.partner_id.name or 'S/N', datos1)  # Cliente/Proveedor
                        worksheet.write(filas,3,entrada, datos3)  # Entrada
                        worksheet.write(filas,4,salida,datos3)  # Salida
                        worksheet.write(filas,5,saldo_incial,datos3_negrita)  # Saldo

        fp = io.BytesIO()
        workbook.save(fp)
        export_id = self.env['xls.extended'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        res = {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'xls.extended',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }
        return res