# -*- coding: utf-8 -*-

{
    'name': "Facturación POS",
    'summary': """
        Facturación Computarizada para el módulo Punto de Venta.
    """,
    'description': """
        Módulo que permite la facturación computarizada bajo normativa boliviana.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1',
    'depends': [
        'codigo_control',
        'point_of_sale',
        'pos_restaurant'
    ],
    'data': [
        'views/pos_config_view.xml',
        'views/point_of_sale.xml',
        'views/pos_session_view.xml',
        'views/pos_order_view.xml',
        'report/views/config_report.xml',
        'report/views/factura_computarizada_rollo.xml',
        'report/views/factura_computarizada_carta.xml',
        'query/configuraciones.sql',
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'application': True,
}