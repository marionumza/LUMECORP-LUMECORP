/**
 * Created by Jose on 10/04/2018.
 */
odoo.define('bamava_pos_facturacion.DB', function (require) {
    "use strict";

    var db = require('point_of_sale.DB');

    db.include({
        _partner_search_string: function(partner) {
            var str =  partner.name;
            if(partner.ean13){
                str += '|' + partner.ean13;
            }
            if(partner.address){
                str += '|' + partner.address;
            }
            if(partner.phone){
                str += '|' + partner.phone.split(' ').join('');
            }
            if(partner.mobile){
                str += '|' + partner.mobile.split(' ').join('');
            }
            if(partner.email){
                str += '|' + partner.email;
            }
            if (partner.
                    nit) {
                str += '|' + partner.nit;
            }
            str = '' + partner.id + ':' + str.replace(':','') + '\n';
            return str;
        },
    });
});