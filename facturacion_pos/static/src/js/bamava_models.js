/**
 * Created by Jose on 06/04/2018.
 * */

odoo.define('bamava_pos_facturacion.models', function (require) {
    "use strict";

    var modules = require('point_of_sale.models');
    var rpc = require('web.rpc');
    // var Model = require('web.DataModel');

    modules.PosModel.prototype.models.forEach(function (p1, p2) {
        if (p1.model == 'res.partner') {
            modules.PosModel.prototype.models[p2].fields = ['name','street','city','state_id','country_id','vat',
                 'phone','zip','mobile','email','barcode','write_date',
                 'property_account_position_id','property_product_pricelist','nit','razon_social']
        };
    });

    modules.PosModel.prototype.push_and_invoice_order = function(order){
        var self = this;
        var invoiced = new $.Deferred();
        if(!order.get_client()){
            invoiced.reject({code:400, message:'Missing Customer', data:{}});
            return invoiced;
        }
        var order_id = this.db.add_order(order.export_as_JSON());
        this.flush_mutex.exec(function(){
            var done = new $.Deferred();
            var transfer = self._flush_orders([self.db.get_order(order_id)], {timeout:30000, to_invoice:true});
            transfer.fail(function(error){
                invoiced.reject(error);
                done.reject();
            });
            transfer.pipe(function(order_server_id){
                rpc.query({model:'dosificacion',method:'nombre_tipo_papel',args:[1]}).then(function(result){
                    var formato_impresion;
                    if (result['nombre'] === 'Rollo'){
                        formato_impresion = 'facturacion_pos.factura_pos';
                    }
                    else if (result['nombre'] === 'Ancho'){
                        formato_impresion = 'facturacion_pos.factura_pos_carta';
                    }
                    self.chrome.do_action(formato_impresion,{additional_context:{
                        active_ids:order_server_id,
                    }}).done(function () {
                        invoiced.resolve();
                        done.resolve();
                    });
                });
            });
            return done;
        });
        return invoiced;
    };

    var orderline_id = 1;

    modules.Orderline = modules.Orderline.extend({
        initialize: function(attr,options){
            this.pos   = options.pos;
            this.order = options.order;
            if (options.json) {
                this.init_from_JSON(options.json);
                return;
            }
            this.product = options.product;
            this.set_product_lot(this.product);
            this.set_quantity(1);
            this.discount = 0;
            this.discountStr = '0';
            this.type = 'unit';
            this.selected = false;
            this.id = orderline_id++;
            this.price_manually_set = false;
            this.tipo_pedido = 'PM';

            if (options.price) {
                this.set_unit_price(options.price);
            } else {
                this.set_unit_price(this.product.get_price(this.order.pricelist, this.get_quantity()));
            }
        },
        get_config_tipo_pedido: function(){
            return this.order.pos.config.agregar_tipo_pedido;
        },

       export_as_JSON: function() {
            var pack_lot_ids = [];
            if (this.has_product_lot){
                this.pack_lot_lines.each(_.bind( function(item) {
                    return pack_lot_ids.push([0, 0, item.export_as_JSON()]);
                }, this));
            }
            return {
                qty: this.get_quantity(),
                price_unit: this.get_unit_price(),
                discount: this.get_discount(),
                product_id: this.get_product().id,
                tax_ids: [[6, false, _.map(this.get_applicable_taxes(), function(tax){ return tax.id; })]],
                id: this.id,
                pack_lot_ids: pack_lot_ids,
                tipo_pedido: this.tipo_pedido ? this.tipo_pedido : '',
            };
       },
    });
    modules.Order = modules.Order.extend({
        export_as_JSON: function() {
            var orderLines, paymentLines;
            orderLines = [];
            this.orderlines.each(_.bind( function(item) {
                return orderLines.push([0, 0, item.export_as_JSON()]);
            }, this));
            paymentLines = [];
            this.paymentlines.each(_.bind( function(item) {
                return paymentLines.push([0, 0, item.export_as_JSON()]);
            }, this));
            return {
                name: this.get_name(),
                amount_paid: this.get_total_paid(),
                amount_total: this.get_total_with_tax(),
                amount_tax: this.get_total_tax(),
                amount_return: this.get_change(),
                lines: orderLines,
                statement_ids: paymentLines,
                pos_session_id: this.pos_session_id,
                pricelist_id: this.pricelist ? this.pricelist.id : false,
                partner_id: this.get_client() ? this.get_client().id : false,
                user_id: this.pos.get_cashier().id,
                uid: this.uid,
                sequence_number: this.sequence_number,
                creation_date: this.validation_date || this.creation_date, // todo: rename creation_date in master
                fiscal_position_id: this.fiscal_position ? this.fiscal_position.id : false
            };
        },
    });
});