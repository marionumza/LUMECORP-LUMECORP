/**
 * Created by Jose on 07/05/2018.
 */
odoo.define('bamava_pos_facturacion.screens', function (require) {
    "use strict";

    var screens = require('point_of_sale.screens');

    screens.OrderWidget.include({
        init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.numpad_state = options.numpad_state;
            this.numpad_state.reset();
            this.numpad_state.bind('set_value',   this.set_value, this);
            this.pos.bind('change:selectedOrder', this.change_selected_order, this);

            this.line_click_handler = function(event){
                self.click_line(this.orderline, event);
                if(event.target.innerHTML.trim() == 'PM'){
                    event.target.innerHTML = 'PLL';
                    self.pos.get("selectedOrder").selected_orderline.tipo_pedido = 'PLL';
                    return;
                }
                else if(event.target.innerHTML.trim() == 'PLL' || event.target.innerHTML.trim() == ''){
                    event.target.innerHTML = 'PM';
                    self.pos.get("selectedOrder").selected_orderline.tipo_pedido = 'PM';
                    return;
                }
            };
            if (this.pos.get_order()) {
                this.bind_order_events();
            }
        },
    })
});