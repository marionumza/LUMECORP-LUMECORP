# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from datetime import datetime
from functools import partial
import logging
_logger = logging.getLogger(__name__)

class PosOrder(models.Model):
    _inherit = 'pos.order'

    dosificacion = fields.Many2one('dosificacion', string='Dosificación', readonly=True)
    monto_pagado = fields.Float(string='Pago de venta', digits=0)

    @api.model
    def _order_fields(self, ui_order):
        result = super(PosOrder, self)._order_fields(ui_order)
        process_line = partial(self.env['pos.order.line']._order_line_fields, session_id=ui_order['pos_session_id'], ui_order=ui_order)
        result['lines'] = [process_line(l) for l in ui_order['lines']] if ui_order['lines'] else False
        result['dosificacion'] = self.env['pos.session'].browse(ui_order['pos_session_id']).dosificacion.id
        result['monto_pagado'] = ui_order['amount_paid']
        return result

    def _prepare_invoice(self):
        result = super(PosOrder, self)._prepare_invoice()
        result['dosificacion'] = self.session_id.dosificacion.id
        result['nit'] = self.partner_id.nit
        result['razon_social'] = self.partner_id.razon_social
        result['date_invoice'] = str(datetime.today())
        result['tipo_factura'] = 'Computarizado'
        return result

    # @api.onchange('statement_ids', 'lines', 'lines')
    # def _onchange_amount_all(self):
    #     for order in self:
    #         currency = order.pricelist_id.currency_id
    #         order.amount_paid = sum(payment.amount for payment in order.statement_ids)
    #         order.amount_return = sum(payment.amount < 0 and payment.amount or 0 for payment in order.statement_ids)
    #         order.amount_tax = currency.round(
    #             sum(self._amount_line_tax(line, order.fiscal_position_id) for line in order.lines))
    #         amount_untaxed = currency.round(sum(line.price_subtotal for line in order.lines))
    #         order.amount_total = order.amount_tax + amount_untaxed

class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    tipo_pedido = fields.Char(string='Tipo Pedido', help='Para la Mesa o para Llevar')
    price_subtotal = fields.Float(string='Subtotal w/o Tax', digits=0, readonly=True, required=True)
    price_subtotal_incl = fields.Float(string='Subtotal', digits=0, readonly=True, required=True)

    def _order_line_fields(self, line, session_id=None, ui_order=None):
        line = super(PosOrderLine, self)._order_line_fields(line,session_id=None)
        if ui_order:
            partner = self.env['res.partner'].browse(ui_order['partner_id'])
            price_list = self.env['product.pricelist'].browse(ui_order['pricelist_id'])
            tax_ids_after_fiscal_position = self.env['account.tax'].search([('id','in',line[2]['tax_ids'][0][2])])
            taxes = tax_ids_after_fiscal_position.compute_all(line[2]['price_unit'], price_list.currency_id, line[2]['qty'], product=line[2]['product_id'], partner=partner)
            if 'price_subtotal' not in line[2]:
                line[2]['price_subtotal'] = taxes['total_excluded']
            if 'price_subtotal_incl' not in line[2]:
                line[2]['price_subtotal_incl'] = taxes['total_included']
        return line