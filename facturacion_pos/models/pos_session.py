# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _

class PosSession(models.Model):
    _inherit = 'pos.session'

    dosificacion = fields.Many2one('dosificacion', string='Dosificación', domain="[('state','=','Activo')]")