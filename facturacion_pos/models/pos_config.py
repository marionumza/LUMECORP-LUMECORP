# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import AccessError, UserError, ValidationError,Warning
import logging
_logger = logging.getLogger(__name__)

class PosConfig(models.Model):
    _inherit = 'pos.config'

    dosificacion = fields.Many2one('dosificacion', string='Dosificación', domain="[('state','=','Activo')]")
    agregar_comandas = fields.Boolean(string="Agregar Comandas")
    agregar_tipo_pedido = fields.Boolean(string="Agregar Comandas")
    mensaje_comandas = fields.Text(string="Mensaje de Pie de Comanda", default='-*- GRACIAS POR SU PREFERENCIA -*-')

    @api.multi
    def open_session_cb(self):
        self.ensure_one()
        if not self.dosificacion:
            raise UserError(_("Por favor seleccione una dosificación para su Punto de Venta."))
        if not self.current_session_id:
            self.current_session_id = self.env['pos.session'].create({
                'user_id': self.env.uid,
                'config_id': self.id,
                'dosificacion': self.dosificacion.id
            })
            if self.current_session_id.state == 'opened':
                return self.open_ui()
            return self._open_session(self.current_session_id.id)
        return self._open_session(self.current_session_id.id)