# -*- coding: utf-8 -*-

{
    'name': "Cobranzas POS",
    'summary': """
        Módulo para registrar los pagos de ventas realizadas por los clientes usando el módulo de POS.
    """,
    'description': """
        Es un módulo de acomplamiento al módulo de cobranzas para registrar los pagos a una sesión de cobranza.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'point_of_sale',
        'cobranzas',
    ],
    'data': [
        'views/pos_config_view.xml',
        'views/pos_order_view.xml',
    ],
    'application': False,
}
