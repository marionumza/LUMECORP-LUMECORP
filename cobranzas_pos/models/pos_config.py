# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import AccessError, UserError, ValidationError,Warning
import logging
_logger = logging.getLogger(__name__)

class PosConfig(models.Model):
    _inherit = 'pos.config'

    def _default_sale_journal(self):
        return self.env['pago.cliente'].search([('state', '=', 'Abierto'), ('responsable', '=', self.env.user.id)], limit=1)

    pago_cliente_id = fields.Many2one('pago.cliente', string='Sesion Cobranza', default=_default_sale_journal)
    cuenta_caja = fields.Many2one('account.account', string='Cuenta Caja/Banco', default=lambda self:self.journal_id.id)
    destination_account_id = fields.Many2one('account.account', string='Cuenta Asignada', default=lambda self: self.env['ir.property'].get('property_account_receivable_id', 'res.partner'))

    @api.multi
    def open_session_cb(self):
        if not self.pago_cliente_id or self.pago_cliente_id.state != 'Abierto':
            raise UserError(_("Por favor seleccione una 'Sesión de Cobranza' en la Configuración de Punto de Venta.\n"
                              "Ó asegúrese de que su 'Sesión de Cobranza' este en estado 'Abierto'."))
        return super(PosConfig, self).open_session_cb()