# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
import logging
_logger = logging.getLogger(__name__)

class PosOrder(models.Model):
    _inherit = 'pos.order'

    def _reconcile_payments(self):
        for order in self:
            aml = order.statement_ids.mapped('journal_entry_ids') | order.account_move.line_ids | order.invoice_id.move_id.line_ids
            for registros in aml:
                if registros.payment_id:
                    registros.payment_id.pago_cliente_id = order.session_id.config_id.pago_cliente_id.id
            aml = aml.filtered(lambda r: not r.reconciled and r.account_id.internal_type == 'receivable' and r.partner_id == order.partner_id.commercial_partner_id)
            aml_returns = aml.filtered(lambda l: (l.journal_id.type == 'sale' and l.credit) or (l.journal_id.type != 'sale' and l.debit))
            try:
                aml_returns.reconcile()
                (aml - aml_returns).reconcile()
            except Exception:
                _logger.exception('Reconciliation did not work for order %s', order.name)