# -*- coding: utf-8 -*-

{
    'name': "Cobranzas (Pagos a Proveedores)",
    'summary': """
        Módulo para registrar los pagos de las compras realizadas a los proveedores y tener un registro visible en el módulo de compras.
    """,
    'description': """
        Es un módulo permite gestionar los pagos a proveedores realizados y tener un control visual de los saldos pendientes en la misma compra.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'purchase',
    ],
    'data': [
        'views/purchase_view.xml',
    ],
    'application': False,
}
