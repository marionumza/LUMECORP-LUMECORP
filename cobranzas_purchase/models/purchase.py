# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID, _
import logging
_logger = logging.getLogger(__name__)

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    pagos = fields.Float(string='Pagos', compute='calcular_saldo')
    saldo = fields.Float(string='Saldo', compute='calcular_saldo')

    @api.multi
    def calcular_saldo(self):
        for order in self:
            if not order.invoice_ids: order.saldo = order.amount_total
            for invoice in order.invoice_ids:
                order.saldo += invoice.residual
            order.pagos = order.amount_total - order.saldo