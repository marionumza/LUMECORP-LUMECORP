# -*- coding: utf-8 -*-
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import UserError, except_orm, ValidationError

import logging
_logger = logging.getLogger(__name__)

class sale_order(models.Model):
	_inherit = 'sale.order'

	rm_move_id = fields.Many2one('account.move', string="Comprobante", help=u'Cuenta que detalle si la venta ya tiene su comprobante generado.', copy=False)

class stock_picking(models.Model):
	_inherit = 'stock.picking'

	rm_return = fields.Boolean(related="move_lines.rm_return",string="Movimiento de retorno", store=True)
	

class stock_move(models.Model):
	_inherit = 'stock.move'

	rm_move_id = fields.Many2one('account.move', string="Comprobante", help=u'Cuenta que detalle si la venta ya tiene su comprobante generado.', copy=False)
	rm_return = fields.Boolean(string="Movimiento de retorno", default=False)


	def unlink(self, cr, uid, ids, context=None):
		context = context or {}
		for move in self.browse(cr, uid, ids, context=context):
			if move.rm_move_id:
				raise UserError(_('No puede eliminar el movimiento, porque existe un regitro contable '+move.rm_move_id.name))
		return super(stock_move, self).unlink(cr, uid, ids, context=context)

	# @api.one
	# def write(self, vals):
	# 	if 'state' in vals:
	# 		if vals['state'] == 'cancel':
	# 			if self.rm_move_id:
	# 				raise UserError(_("No puede Cancelar la nota, porque tiene un registro contable " + self.rm_move_id.name))
	# 	result = super(stock_move, self).write(vals)
	# 	return result