# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
import re
import uuid
import logging

_logger = logging.getLogger(__name__)

class AccountBankStatementLine(models.Model):
	_inherit = "account.bank.statement.line"

	rm_move_id = fields.Many2one('account.move', string="Comprobante", help=u'Cuenta que detalle si la venta ya tiene su comprobante generado.')

class AccountInvoice(models.Model):
	_inherit = "account.invoice"

	move_id = fields.Many2one('account.move', string='Journal Entry',
		readonly=True, index=True, ondelete='restrict', copy=False,
		help="Link to the automatically generated Journal Items.")
	number = fields.Char(string='Number', readonly=True, copy=False)

	@api.one
	@api.depends(
		'state', 'currency_id', 'invoice_line_ids.price_subtotal',
		'move_id.line_ids.amount_residual',
		'move_id.line_ids.currency_id')
	def _compute_residual(self):
		super(AccountInvoice, self)._compute_residual()
		self.reconciled = False

	@api.multi
	def invoice_validate(self):
		for invoice in self.filtered(lambda invoice: invoice.partner_id not in invoice.message_partner_ids):
			invoice.message_subscribe([invoice.partner_id.id])

			# Auto-compute reference, if not already existing and if configured on company
			if not invoice.reference and invoice.type == 'out_invoice':
				invoice.reference = invoice._get_computed_reference()

			# DO NOT FORWARD-PORT.
			# The reference is copied after the move creation because we need the move to get the invoice number but
			# we need the invoice number to get the reference.
			# invoice.move_id.ref = invoice.reference
		self._check_duplicate_supplier_reference()

		return self.write({'state': 'open'})

	@api.multi
	def action_move_create(self):
		for inv in self:
			ctx = dict(self._context, lang=inv.partner_id.lang)
			dic = {}
			if not inv.date_invoice:
				dic['date_invoice'] = fields.Date.context_today(self)
			else:
				dic['date_invoice'] = inv.date_invoice
			if not inv.date_due:
				dic['date_due'] = dic['date_invoice']
			dic['date'] = dic['date_invoice']
			if inv.journal_id.sequence_id:
				inv.number = inv.journal_id.sequence_id.with_context(ir_sequence_date=dic['date_invoice']).next_by_id()
				# dic['number'] = inv.number
			if dic:
				inv.write(dic)
				# inv.with_context(ctx).write(dic)
			_logger.info('\n\n %r \n\n',dic)
		return True

	@api.multi
	def action_cancel(self):
		moves = self.env['account.move']
		for inv in self:
			# if inv.move_id:
			# 	if moves.state==''
			# 	moves += inv.move_id
			if inv.payment_move_line_ids:
				raise UserError(_('You cannot cancel an invoice which is partially paid. You need to unreconcile related payment entries first.'))

		# First, set the invoices as cancelled and detach the move ids
		self.write({'state': 'cancel'})
		# self.write({'state': 'cancel', 'move_id': False})
		# if moves:
		# 	# second, invalidate the move(s)
		# 	moves.button_cancel()
		# 	# delete the move this invoice was pointing to
		# 	# Note that the corresponding move_lines and move_reconciles
		# 	# will be automatically deleted too
		# 	moves.unlink()
		return True