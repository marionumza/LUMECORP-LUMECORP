# -*- coding: utf-8 -*-
##############################################################################

from odoo import api, fields, models, tools, _
from datetime import datetime, timedelta, date
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

import logging
_logger = logging.getLogger(__name__)

# Lista de tipos de registros de Caja Chica
# REGISTROS DE CAJA CHICA
TIPO_PAGO = [('Retencion','Retencion'),
	('Anticipo','Anticipo'),
	('Pago','Pago a proveedor'),
	('Factura','Factura'),
	('Factura No Deducible','Factura no deducible'),
	('No Deducible','No deducible'),
	('Ingreso','Ingreso'),
	('Egreso', 'Egreso'),
	('Valido', 'Valido'),
	('Anulado', 'Anulado')]

# Creando objetos de los modelos (sale.order, stock.picking, purchase.order, account.invoice) 
# Para que se creen las estructuras.
# http://prntscr.com/jopwl3
# listamodelos=[('sale.order','Ventas'),('stock.picking','Inventarios'),('purchase.order','Compras')]
class ng_asiento(models.Model):
	_name =  "ng.asiento"
	_description = 'Modelo para el comprobante'

	# obtiene el nombre del modelo y lo escribe como titulo.
	@api.onchange('rm_modelo')
	def change_nombre_asiento(self):
		self.name = self.rm_modelo.name or ""

	# domain para filtrar el modelo, todos menos los modelos
	# que empiezan con las siquientes palabras (ir|base|work|report|bus|mail|web|wiz|chan|cash|db|email)
	@api.onchange('name')
	def change_modelos(self):
		cr = self._cr
		cr.execute("""
			SELECT
			id
			FROM ir_model
			WHERE id NOT IN (
			SELECT
			ir1.id
			FROM ir_model as ir1
			WHERE ir1.model similar to '(ir|base|work|report|bus|mail|web|wiz|chan|cash|db|email)%')
			ORDER BY ir_model.model;
			""")
		result_query = [x[0] for x in cr.fetchall()]
		return {'domain':{'rm_modelo' : [('id', 'in', result_query)]}}

	name = fields.Char(string="Nombre asiento", required=True)
	rm_modelo = fields.Many2one('ir.model', string="Modelo", help=u'Lista de modelos')

# Crea el objeto de la estructura de comprobantes.
# http://prntscr.com/joq5vo
class rm_estructura_comprobante(models.Model):
	_name =  'ng.estructura.comprobante'
	_description = "Estructura de comprobante"
	_rec_name = "name"

	# Campos de la estructura.
	name = fields.Char(string="Estructura",help=u"Name de la Estructura")
	rm_tipo_estructura = fields.Many2one("ng.asiento", string="Asiento")
	rm_modelo = fields.Many2one(related='rm_tipo_estructura.rm_modelo', store=True, readonly=True, string="Modelo")
	rm_agrupar_total = fields.Boolean(string="Agrupar asientos")
	rm_serie = fields.Many2one( "account.journal",string="Serie",required=True)
	rm_concepto = fields.Char(string='Concepto',required=True, help="Glosa o descripcion que se registrara en el comprobante")
	# Campo de la estructura
	rm_estructura = fields.One2many( "ng.estructura.detalle", "estructura_id", String="Estructura")
	active = fields.Boolean(String="Activo", default=True)
	rm_campo = fields.Many2many('ir.model.fields', 'fields_campos', 'estruc', 'field', string='Valor', help=u'Valor / campo que se usara para realizar la operacion.')
	rm_campo_aux = fields.Many2many('ir.model.fields','fields_campos_aux', 'estruc_aux', 'field_aux', string='Valor auxiliar', help=u'Valor / campo auxiliar que se usara para realizar alguna operacion.')
	rm_cuenta_analitica = fields.Many2one('ir.model.fields', string='Cuenta analitica')

	# Campo de los parametros/filtros
	rm_parametros = fields.One2many('ng.parametros', 'estructura_id', string="Parametros", help="Parametros que tendra la estructura para generar el comprobante")
	rm_campo_sumatorio = fields.Boolean(string="Valor sumatorio", default=False)
	rm_campo_many = fields.Many2one('ir.model.fields', string='Campo sumatorio', help=u'Campo que se usara en conjunto. de suma del campo en especifico')
	rm_model_sum = fields.Char(related='rm_campo_many.relation',store=True, readonly=True, string='Modelo', help=u'Campo que se usara para realizar la operacion. de suma del campo en especifico')
	rm_campo_sum = fields.Many2many('ir.model.fields', 'fields_campos_suma', 'estruc', 'field', string='Valor', help=u'Valor / campo que se usara para realizar la operacion.')

	# si cambie el modelo de la estructura entonces el nombre cambia.
	# http://prntscr.com/joq801
	@api.onchange('rm_tipo_estructura')
	def change_tipo_estructura(self):
		modelo = ''
		for datos in self:
			for estructura in datos.rm_estructura:
				estructura.rm_modelo_aux = datos.rm_tipo_estructura.rm_modelo.model
			for params in datos.rm_parametros:
				params.rm_modelo = datos.rm_tipo_estructura.rm_modelo.model

			datos.name = datos.rm_tipo_estructura.name if datos.rm_tipo_estructura else ''
			datos.rm_campo = False
			datos.rm_campo_aux = False

# Modelo para definir los parametros/filtros de la estructura del comprobante.
class rm_parametros(models.Model):
	_name = "ng.parametros"
	_description = "Parametros para el comprobante"
	_rec_name = "rm_field"

	# si cambia el parametro entonces, el campo de valor se actualiza
	# El objeto ng.tabla.auxiliar es axuliar para almacenar el valor del campo field o parametro
	# http://prntscr.com/joqjd3
	@api.onchange('rm_field')
	def _selection_parametros(self):
		array = []
		cr = self._cr
		if self.rm_field:
			cr.execute("DELETE FROM ng_tabla_auxiliar;")
			if self.rm_field.ttype == 'many2one':
				objeto = (self.rm_field.relation).replace('.','_')
				try:
					cr.execute("SELECT id,name FROM " + objeto)
					array = [self.env['ng.tabla.auxiliar'].create({'name':x[1],'key_int':x[0]}) for x in cr.fetchall()]
				except ValueError:
					raise UserError(_("No existe el campo NAME en el objeto "+objeto + " Por favor contacte con su administrador de sistemas"))
			elif self.rm_field.ttype == 'boolean':
				self.env['ng.tabla.auxiliar'].create({'name':'False','key_boolean':False})
				self.env['ng.tabla.auxiliar'].create({'name':'True','key_boolean':True})
			elif self.rm_field.ttype == 'selection':
				objeto = (self.rm_field.model).replace('.','_')
				# Esto es si existe el modelo de registros de tesoreria(CAJA CHICA) tipo selection
				try:
					if 'rm_registros_tesoreria' in objeto:
						array = [self.env['ng.tabla.auxiliar'].create({'name':x[0],'key_char':x[0]}) for x in TIPO_PAGO]
					else:
						cr.execute("SELECT DISTINCT("+self.rm_field.name+") FROM " + objeto)
						array = [self.env['ng.tabla.auxiliar'].create({'name':x[0],'key_char':x[0]}) for x in cr.fetchall()]
				except ValueError:
					raise UserError(_("No existe el campo NAME en el objeto "+objeto + "Por favor contacte con su administrador de sistemas"))
		return {'required':{'rm_valor':True if array else False}}

	# El campo del modelo es dependiente del padre 
	# http://prntscr.com/joqlq5
	@api.depends('rm_field')
	def depends_campo(self):
		for datos in self:
			datos.rm_modelo=datos.estructura_id.rm_tipo_estructura.rm_modelo.model

	# almacena el valor seleccionado del parametro 
	# http://prntscr.com/joqo7j
	@api.onchange('rm_valor')
	def change_valor(self):
		if self.rm_valor:
			if self.rm_valor.key_char:
				self.rm_key = self.rm_valor.key_char
			elif self.rm_valor.key_boolean:
				self.rm_key = str(self.rm_valor.key_boolean)
			elif self.rm_valor.key_int:
				self.rm_key = str(self.rm_valor.key_int)
			self.name = self.rm_valor.name
			self.name_aux = self.rm_valor.name
		self.rm_valor = None

	estructura_id = fields.Many2one("ng.estructura.comprobante", string="Id estructura")
	rm_field = fields.Many2one('ir.model.fields', string='Parametro', required=True, help=u'Campo que se usara como parametro.')
	rm_modelo = fields.Char(string="Modelo", store=True, readonly=True, compute='depends_campo')
	rm_valor = fields.Many2one('ng.tabla.auxiliar', string="Parametro")
	name = fields.Char(string="Valor")
	name_aux = fields.Char(string="Valor")
	rm_key = fields.Char(string="key")

	# Como es readonly el campo name (valor) entonces se lo vuelve a reescribir al crear
	@api.model
	def create(self,vals):
		vals['name'] = vals['name_aux']
		return super(rm_parametros, self).create(vals)

	@api.multi
	# Como es readonly el campo name (valor) entonces se lo vuelve a reescribir al modificar
	def write(self,vals):
		if 'rm_key' in vals:
			vals['name'] = vals['name_aux']
		return super(rm_parametros, self).write(vals)

# Crea el objeto del detalle o la estructura del asiento contable
# http://prntscr.com/joqqqo
class rm_detalle_estructura(models.Model):
	_name =  "ng.estructura.detalle"
	_description="Detalle de estructura de asiento"
	_rec_name = 'estructura_id'

	# hereda el modelo del objeto padre
	# Para que pueda realizar filtros en los parametros.
	@api.depends('rm_cuenta_contable')
	def depends_cuenta_contable(self):
		for datos in self:
			# if datos.rm_cuenta_contable:
			datos.rm_modelo_aux=datos.estructura_id.rm_tipo_estructura.rm_modelo.model

	# Busca los modelos relacionados al padre 
	# Que lo registra com un domain / filtro del campo rm_modelo
	@api.onchange('rm_modelo_aux')
	def _onchange_lista(self):
		def buscar_modelos(cod_modelo):
			cr = self._cr
			consulta2="""select DISTINCT(ir_model.id) from ir_model INNER JOIN ir_model_fields imf on imf.relation=ir_model.model where  imf.model_id= %s and imf.relation NOTNULL"""
			if cod_modelo:
				cr.execute(consulta2,([cod_modelo]))
				resultado2 = [i[0] for i in cr.fetchall()]
			return resultado2

		cr = self._cr
		lista_modelos_hijos=[]
		consulta="""select DISTINCT(ir_model.id) from ir_model INNER JOIN ir_model_fields imf on imf.relation=ir_model.model where  imf.model= %s and imf.relation NOTNULL"""
		if self.rm_modelo_aux:
			cr.execute(consulta,([self.rm_modelo_aux]))
			resultado = [i[0] for i in cr.fetchall()]
			for cod_modelo in resultado:
				lista_modelos_hijos=lista_modelos_hijos + buscar_modelos(cod_modelo)
		
			id_modelo=self.env['ir.model'].search([('model','=',self.rm_modelo_aux)]).ids
			return {'domain':{'rm_modelo':[('id','in',resultado+id_modelo+lista_modelos_hijos)]}}
		else:

			return {'domain':{'rm_modelo':[('id','in',[0])]}}

	# Si elige un cuenta entonces no puede elegir un modelo y su cuenta del modelo
	# Elije una cuenta de forma estatica.
	# http://prntscr.com/jorcdq
	@api.onchange('rm_cuenta_contable')
	def change_cuenta_contable(self):
		if self.rm_cuenta_contable:
			self.rm_modelo = False
			self.rm_campo = False

	# Si elije un modelo para que obtenga la cuenta contable dependiente del modelo
	# http://prntscr.com/jorczg
	@api.onchange('rm_modelo')
	def change_modelo(self):
		self.rm_campo = False
		if self.rm_modelo:
			self.rm_cuenta_contable = False

	# solo puede registar debe y no haber en una sola linea
	@api.onchange('rm_debe')
	def change_debe(self):
		if self.rm_debe:
			self.rm_haber = False

	# solo puede registar haber y no debe en una sola linea
	@api.onchange('rm_haber')
	def change_haber(self):
		if self.rm_haber:
			self.rm_debe = False


	estructura_id = fields.Many2one("ng.estructura.comprobante", string="Id Automatico")
	rm_cuenta_contable = fields.Many2one('account.account', string="Cuenta", help=u'Cuenta contable del registro.')
	rm_modelo = fields.Many2one('ir.model',string="Modelo", help=u'Lista de Modulos que obtendra los datos para obtener las cuentas')
	rm_modelo_aux = fields.Char(string="Modelos", store=True, readonly=True, compute='depends_cuenta_contable')
	rm_campo = fields.Many2one('ir.model.fields',string='Cuenta del modelo', help=u'Campo de donde se obtendra la cuenta del modelo')
	rm_debe = fields.Many2one( "ng.tipo.operacion", string="Debe", help=u'Operacion que realizara')
	rm_haber = fields.Many2one( "ng.tipo.operacion", string="Haber", help=u'Operacion que realizara')
	rm_operacion = fields.Selection([('Neto','Neto'),('Descuento','Descuento'),('Bruto','Bruto'),('Multiplicacion','Multiplicacion'),('Suma','Suma'),('Resta','Resta'),('Division','Division')], string="Tipo de operacion", required=True,default='Bruto', help=u'Tipo de operacion que se desea realizar con los valores seleccionados.')
	rm_glosa = fields.Char(string="Glosa", help=u'Informacion de la glosa del detalle del comprobante.', required=True)
	rm_descripcion = fields.Char(string="Descripcion", help=u'Descripcion del registro.')
	rm_campo_auxiliar = fields.Char(string='Campo auxiliar', 
		help=u"Glosa configurable del asiento\nEjemplo\n1.- Del formulario de Importacion de Compras\
			se requiere como auxiliar el campo 'pedido de compra' su valor es PO00001, entonces se debe de configurar ${object.name}\n 2.- Del fomulario de costo de importacion\
			2.- Del fomulario de costo de importacion se require como auxiliar el campo 'Compra' su valor es PO00001, entonces se debe de configurar ${object.purchase_order_id.name}")
	# rm_campo_auxiliar = fields.Many2one('ir.model.fields', string='Campo auxiliar')

	# Al momento de crear el campo axuliar tiene que tener solo un formato ${object.nombre_campo} eso lo crea como auxiliar en los comprobantes
	@api.model
	def create(self, vals):
		if vals.get('rm_campo_auxiliar'):
			vals['rm_campo_auxiliar'] = vals['rm_campo_auxiliar'].strip()
			if vals['rm_campo_auxiliar'] == '':
				vals['rm_campo_auxiliar'] = None
			elif "${object" not in vals['rm_campo_auxiliar']:
				raise UserError(u"Formato correcto aceptable ${object.nombre_campo}, en el Campo auxiliar de la ESTRUCTURA")
		return super(rm_detalle_estructura, self).create(vals)

	# Al momento de modificar el campo axuliar tiene que tener solo un formato ${object.nombre_campo} eso lo crea como auxiliar en los comprobantes
	@api.multi
	def write(self, vals):
		if vals.get('rm_campo_auxiliar'):
			vals['rm_campo_auxiliar'] = vals['rm_campo_auxiliar'].strip()
			if vals['rm_campo_auxiliar'] == '':
				vals['rm_campo_auxiliar'] = None
			elif "${object" not in vals['rm_campo_auxiliar']:
				raise UserError(u"Formato correcto aceptable ${object.nombre_campo}, en el Campo auxiliar de la ESTRUCTURA")
		return super(rm_detalle_estructura, self).write(vals)


# Creando objeto de tipos de operaciones
# Tipo de operacion o calculo que se realizara con el el monto total del objeto
# Multiplicacion
# Resta
# Suma
# Excentos en IVA, etc...
class rm_tipo_operacion(models.Model):
	_name =  "ng.tipo.operacion"
	_description = 'Operaciones'
	_order = 'name'

	name = fields.Char(string="Nombre Operacion", required=True)
	rm_operacion = fields.Selection([('igu','='),('sum','+'),('res','-'),('multi','*'),('div','/'),('porcentaje','%')],string="Operaciones",default="porcentaje")
	rm_valor = fields.Float(string="Valor de la operacion", required=True)

	_sql_constraints = [
		('mane_uniq', 'unique (name,rm_valor)', 'El nombre y/o valor tiene que ser unico!')
	]

# Crea el objeto para una tabla auxiliar
# almacena temporalmente el tipo de dato y el valor
class rm_tabla_auxiliar(models.Model):
	_name = "ng.tabla.auxiliar"
	_descripcion="Tabla auxiliar de parametros para la estructura del comprobante"
	_order = 'name'
	_rec_name = 'name'

	name = fields.Char(string="Tipo")
	key_char = fields.Char(string='Key')
	key_boolean = fields.Boolean(string='Key boolean')
	key_int = fields.Integer(string='Key Integer')