# -*- coding: utf-8 -*-
##############################################################################
from openerp import api, fields, models, _
from openerp.tools.translate import _
from openerp.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class stock_config_settings(models.TransientModel):
	_inherit = 'stock.config.settings'

	rm_estructura_contable = fields.Many2one('ng.estructura.comprobante', 
		string='Estructura contable de devolucion',
		help="Tipo de factura al momento de crear un compra o una compra y facturas de comrpas.")

	@api.multi
	def set_estructura_contable(self):
		res = self.env['ir.values'].set_default('stock.config.settings', 'rm_estructura_contable', self.rm_estructura_contable.id)
		return res

	@api.multi
	def set_estructura_contable1(self):
		self.env['ir.values'].write({'rm_estructura_contable': self.rm_estructura_contable.id})