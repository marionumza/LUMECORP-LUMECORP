{
    'name' : "NG - Contabilidad",
    'author' : "Gustavo H.",
    'version' : "0.1",
    'description': """
Mejoras para el Módulo de Contabilidad
===========================

Modulo para generar comprobantes automaticos
    """,
    'category' : "Account",
    'depends' : ['account', 'account_cancel','sale', 'purchase', 'stock'],
    'website': 'https://www.odoo.com',
    'data' : [
              'data/rm_tipos_operaciones.xml',
              'security/ir.model.access.csv',
              'views/rm_contabilidad_automatica_view.xml',
              'views/rm_picking.xml',
              'views/rm_sale.xml',
              'wizard/rm_generar_asiento_wiz_view.xml',
              'views/rm_menu.xml',
             ],
    'demo' : [],
    'installable': True,
    'auto_install': False
}
