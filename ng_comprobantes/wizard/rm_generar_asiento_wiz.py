#!/usr/bin/env python
# -*- coding: 850 -*-
# -*- coding: utf-8 -*-
# -*- coding: latin-1 -*-
##############################################################################

from odoo import api, fields, models, tools, _
from odoo import tools
import pytz
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

import logging
_logger = logging.getLogger(__name__)

# Crea el objeto wizard para poder generar los asientos contables.
class rm_generar_asiento_wiz(models.Model):
	_name = 'ng.generar.asiento.wiz'
	_description="Reporte de asientos"

	fecha_inicial = fields.Date('De la Fecha', required=True,default=lambda *a: (datetime.now() - timedelta(hours = 4)).strftime('%Y-%m-%d'))
	fecha_final = fields.Date('Hasta la Fecha', required=True,default=lambda *a: (datetime.now() - timedelta(hours = 4)).strftime('%Y-%m-%d'))
	tipo_estructura = fields.Many2one('ng.estructura.comprobante',string="Asiento")
	asesor_usuario = fields.Many2one('res.users', string='Usuario')

	# Funcion para obtener las horas corectas de la zona horaria
	# especificamente la zona horaria del usuario que tiene que tener configurado. http://prntscr.com/jorlht
	# tambien del sistema http://prntscr.com/jorlxu
	@api.multi
	def _get_utc_time_range(self, form,date1,date2):
		user = self.env['res.users'].browse(self.env.uid)
		tz_name = user.tz or self.localcontext.get('tz') or 'UTC'
		user_tz = pytz.timezone(tz_name)
		between_dates = {}
		for date_field, delta in {date1: {'days': 0}, 'fecha_fin': {'days': 1}}.items():
			timestamp = datetime.strptime(form[date_field] + ' 00:00:00', tools.DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(**delta)
			timestamp = user_tz.localize(timestamp).astimezone(pytz.utc)
			between_dates[date_field] = timestamp.strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)
		return between_dates[date1], between_dates[date2]

	# Boton generar asiento
	@api.multi
	def generar_asiento(self):

		# ids para los comprobantes generados
		comprobante = []
		
		# estructura comprobantes
		tipo_estructura = self.tipo_estructura
		if not self.tipo_estructura:
			tipo_estructura = self.env['ng.estructura.comprobante'].search([('active','=',True)])

		# rango de fechas que va a generar el comprobante
		fecha1 = self.fecha_inicial
		fecha2 = self.fecha_final
		def daterange(d1, d2):
			return (d1 + timedelta(days=i) for i in range((d2 - d1).days + 1))

		# Esto es para generar un determinado rango de fechas de fechas
		for fechas in daterange(fecha1,fecha2):
			fecha = fechas.strftime('%Y-%m-%d')
			# Bucle para obtener los comprobantes generados segun la estructura elegida por fecha
			for estructura in tipo_estructura:
				bandera_fecha = True
				parametro = []
				sale_jornal_id = 0
				bandera_pagos = False
				dic = {}
				# crea un diccionario de array de los filtros o parametros de la estructura
				for filtros in estructura.rm_parametros:
					if filtros.rm_field.ttype in ['date','datetime']:
						continue
					dic[filtros.rm_field.name] = []
				# Almacena los valores de los parametros segun el tipo de dato, en el diccionario para usar como parametros
				for filtros in estructura.rm_parametros:
					if bandera_fecha and filtros.rm_field.ttype in ['date','datetime']:
						fecha_inicio = fecha
						fecha_fin = fecha
						if filtros.rm_field.ttype == 'datetime':
							fecha_inicio, fecha_fin = self._get_utc_time_range({filtros.rm_field.name: fecha, 'fecha_fin':fecha},filtros.rm_field.name,'fecha_fin')
						parametro.append((filtros.rm_field.name,'>=',fecha_inicio))
						parametro.append((filtros.rm_field.name,'<=',fecha_fin))
						bandera_fecha = False
					elif filtros.rm_field.ttype == 'many2one':

						if filtros.rm_key:
							dic[filtros.rm_field.name].append(int(filtros.rm_key))
						else:
							parametro.append((filtros.rm_field.name,'=',None))
					else:
						if filtros.rm_field.ttype == 'boolean':
							dic[filtros.rm_field.name].append(str(filtros.rm_key) if str(filtros.rm_key) == 'True' else None)
						else:
							dic[filtros.rm_field.name].append(str(filtros.rm_key))

				# obtiene los datos almacenados del diccionado de parametros, para que lo convierta en filtros de busqueda del objeto
				for key,value in dic.items():
					if value:
						if 'sale_journal' == key:
							sale_jornal_id = value
							bandera_pagos=True
						else:
							parametro.append((key,'in',value))
				# obtiene el objeto del la estrucuta para que realize la busqueda con los filtros
				objeto_comprobante = self.env[estructura.rm_tipo_estructura.rm_modelo.model]
				# tiene que existir uno de estos campos para que pueda almacenar el comprobante creado, asi tambien como filtro
				# rm_move_id
				# account_move
				# move_id
				if 'rm_move_id' in objeto_comprobante._fields:
					parametro.append(('rm_move_id','=',None))
					objeto_comprobante = objeto_comprobante.search(parametro)
				# Esto es para POS o punto de venta
				elif 'account_move' in objeto_comprobante._fields:
					# parametro.append(('account_move','=',None))
					objeto_comprobante = objeto_comprobante.search(parametro)
				elif 'move_id' in objeto_comprobante._fields:
					parametro.append(('move_id','=',None))
					objeto_comprobante = objeto_comprobante.search(parametro)
				else:
					raise UserError(_("Por favor cree un campo para almacenar el comprobante en el objeto "+ estructura.rm_tipo_estructura.rm_modelo.name+ ", Por favor contacte con su Administrador de Sistemas"))
				pagos_search = []
				# Esto es para estructuras del POS
				# Realiza una suma total diferenciado por tipos de pagos y asi crear asientos por tipos de pagos (EFECTIVO, TARJETAS DE DEBITO)
				if bandera_pagos:
					pagos_search_pos = self.env['account.bank.statement.line'].search([('journal_id','in',sale_jornal_id),('pos_statement_id','in',objeto_comprobante.ids),('rm_move_id','=', None)])
					lista = [pagos.pos_statement_id.id for pagos in pagos_search_pos]
					pagos_search = sale_jornal_id
					lista = list(set(lista))
					objeto_comprobante = objeto_comprobante.browse(lista)
					if estructura.rm_campo_sumatorio:
						lista = []
						for objeto in objeto_comprobante:
							ids_relacional = objeto.read()[0][estructura.rm_campo_many.name]
							campo_onetomany = self.env[estructura.rm_campo_many.relation].browse(ids_relacional)
							# if bandera_pagos:
							for valor in campo_onetomany:
								# Esta bandera solo es para POS por detalle de cobros
								if valor.journal_id.id in sale_jornal_id:
									lista.append(objeto.id)
						lista = list(set(lista))
						objeto_comprobante = self.env[estructura.rm_tipo_estructura.rm_modelo.model].browse(lista)

				# si no encuentra ningun objeto entonces pasa a la siguiente fecha
				if not objeto_comprobante:
					continue
				# Crea en un solo comprobante por fecha, esto si en la estructura esta seleccionado el campo Agrupar asientos
				# http://prntscr.com/jorrkq
				if not estructura.rm_agrupar_total:
					for objeto in objeto_comprobante:
						id_comprobante = self.generar_comprobante(objeto, estructura,fecha,estructura.rm_tipo_estructura.rm_modelo.model, pagos_search, bandera_pagos)
						# Verifica que exista un campo de tipo account.move para que pueda almacenar el id de la creacion del comprobante.
						if 'rm_move_id' in objeto._fields:
							objeto.rm_move_id = id_comprobante[0]
							# Esto es para POS o punto de venta
						elif 'account_move' in objeto._fields:
							if estructura.rm_campo_sumatorio:
								ids_relacional = objeto.read()[0][estructura.rm_campo_many.name]
								campo_onetomany = self.env[estructura.rm_campo_many.relation].browse(ids_relacional)
								for valor in campo_onetomany:
									# Esta bandera solo es para POS por detalle de cobros
									if valor.journal_id.id in sale_jornal_id:
										valor.rm_move_id = id_comprobante[0]
							else:
								objeto.account_move = id_comprobante[0]
						elif 'move_id' in objeto._fields:
							objeto.move_id = id_comprobante[0]
						# va agrupando los IDs creados de los comprobantes para que al final de la operacion se visualicen.
						comprobante += id_comprobante
				else:
					# Caso contrario crear un comprobante por objeto o transaccion
					id_comprobante = self.generar_comprobante(objeto_comprobante, estructura,fecha,estructura.rm_tipo_estructura.rm_modelo.model, pagos_search, bandera_pagos)
					for objeto in objeto_comprobante:
						if 'rm_move_id' in objeto._fields:
							objeto.rm_move_id = id_comprobante[0]
							# Esto es para POS o punto de venta
						elif 'account_move' in objeto._fields:
							if estructura.rm_campo_sumatorio:
								ids_relacional = objeto.read()[0][estructura.rm_campo_many.name]
								campo_onetomany = self.env[estructura.rm_campo_many.relation].browse(ids_relacional)
								for valor in campo_onetomany:
									# Esta bandera solo es para POS por detalle de cobros
									if valor.journal_id.id in sale_jornal_id:
										valor.rm_move_id = id_comprobante[0]
							else:
								objeto.account_move = id_comprobante[0]
							objeto.account_move = id_comprobante[0]
						elif 'move_id' in objeto._fields:
							objeto.move_id = id_comprobante[0]
						# objeto.rm_move_id = id_comprobante[0]
					comprobante += id_comprobante

		# Retorna en formato vista los comprobantes creados
		action = self.env.ref('account.action_move_journal_line')
		result = action.read()[0]
		result['context'] = {}
		result['domain'] = "[('id', 'in', " + str(comprobante) + ")]"
		return result

	# funcion para crear los comprobantes
	# objeto > enviar el objeto y su ID encontrado. ejemplo sale.order(1)
	# tipo_estructura > envia el objeto de la estructura para generar el comprobante
	# fecha_comprobante > Envia la fecha correspondiente a la busqueda del objeto ejemplo si fue una venta del 1ro de enero entonces envia la fecha 2018-01-01
	# nombre_objeto > envia el nombre del objeto o modelo emjemplo 'sale.order'
	# pagos_search > parametro que le envia solo estructuras de POS
	# bandera_pagos > parametro que le envia solo estructuras de POS
	@api.multi
	def generar_comprobante(self, objeto, tipo_estructura, fecha_comprobante, nombre_objeto, pagos_search, bandera_pagos):
		dic_cuentas = {}
		# Crea un diccionario de cuentas para que pueda agrupar por cuenta contable la informacion de cada comprobante.
		for ids in tipo_estructura.rm_estructura:
			if ids.rm_cuenta_contable:
				dic_cuentas[ids.rm_cuenta_contable.id] = [0,0,0,0,{},'',None,0]

		# Obtiene la tasa actual para que pueda calcular con la divisa correspondiente.
		fecha = (datetime.now()).strftime('%Y-%m-%d %H:%M:%S')
		tasa = self.obtener_tasa(fecha)
		tasa_actual = tasa

		detalle = lista = []
		cuenta = 0
		# Esto es para obtener el total de la transaccion
		valores = {}
		for datos in objeto:
			if not tipo_estructura.rm_campo_sumatorio:
				total,total_aux,tasa = self.totales_del_objeto(tipo_estructura.rm_campo,tipo_estructura.rm_campo_aux,datos,datos,tasa)
			else:
				ids_relacional = datos.read()[0][tipo_estructura.rm_campo_many.name]
				campo_onetomany = self.env[tipo_estructura.rm_campo_many.relation].browse(ids_relacional)
				total = 0.0
				for valor in campo_onetomany:
					# Esta bandera solo es para POS
					if bandera_pagos:
						total_sum = 0
						if valor.journal_id.id in pagos_search:
							total_sum,total_aux,tasa = self.totales_del_objeto(tipo_estructura.rm_campo_sum,[],valor,None,tasa)
					else:
						total_sum,total_aux,tasa = self.totales_del_objeto(tipo_estructura.rm_campo_sum,[],valor,None,tasa)
					total += total_sum
					# total_aux_sum += total_aux
				total_no,total_aux,tasa = self.totales_del_objeto([],tipo_estructura.rm_campo_aux,None,datos,tasa)
			valores[datos.id] = [total,total_aux]
		for datos in objeto:
			# Obteniendo el cliente del modelo res_partner
			partner_id = self.obtener_cliente(datos)

			total = valores[datos.id][0]
			auxiliar = valores[datos.id][1]
			# Realiza un recorrido por todo la estructura http://prntscr.com/jorxo5
			for estructura in tipo_estructura.rm_estructura:
				debe = estructura.rm_debe
				haber = estructura.rm_haber
				tipo_operacion = estructura.rm_operacion
				
				if debe:
					total_debe = total
					total_haber = 0.0
				else:
					total_debe = 0.0
					total_haber = total

				valor_debe = self.realizar_operaciones(total_debe,debe.rm_valor or 0,debe.rm_operacion or 'ninguno',tipo_operacion,auxiliar,tasa,nombre_objeto,tipo_estructura.rm_estructura)
				valor_haber = self.realizar_operaciones(total_haber,haber.rm_valor or 0,haber.rm_operacion or 'ninguno',tipo_operacion,auxiliar,tasa,nombre_objeto,tipo_estructura.rm_estructura)

				if ((valor_debe > 0.0) and (valor_haber > 0.0)) and ((valor_debe == 0.0) and (valor_haber == 0.0)):
					break
				elif (valor_debe < 0.0) or (valor_haber < 0.0):
					break
				if estructura.rm_cuenta_contable:
					cuenta = estructura.rm_cuenta_contable.id
				else:
					if not estructura.rm_campo:
						raise UserError(_('Por favor ingrese un cuenta en la estructura ' + tipo_estructura.name))
					cuenta = datos.read()[0][estructura.rm_campo.name]
					if not cuenta:
						raise UserError(_('Registre una Cuenta en ' + datos.read()[0]['display_name']))
					cuenta = self.env['account.account'].browse(cuenta[0]).id
					if cuenta not in dic_cuentas:
						dic_cuentas[cuenta] = [0,0,0,0,{},'',None,0]

				if debe.rm_operacion == 'igu' or haber.rm_operacion == 'igu':
					dic_cuentas[cuenta][4][datos.id] = [valor_debe,valor_haber,(valor_debe / tasa_actual),(valor_haber / tasa_actual),partner_id or None,estructura.rm_glosa]
					
					detalle.append(self.detalle_comprobante([valor_debe,valor_haber,(valor_debe / tasa_actual),(valor_haber / tasa_actual)],(partner_id.id if partner_id else None)
						,estructura.rm_glosa,cuenta,estructura.rm_campo_auxiliar, datos.ids, fecha_comprobante,nombre_objeto,tipo_estructura))
				else:
					dic_cuentas[cuenta][0] += valor_debe
					dic_cuentas[cuenta][1] += valor_haber
					dic_cuentas[cuenta][2] += (valor_debe / tasa_actual)
					dic_cuentas[cuenta][3] += (valor_haber / tasa_actual)
					dic_cuentas[cuenta][5] = estructura.rm_glosa
					#ESTO ES PARA CAMPOS AUXILIARES
					dic_cuentas[cuenta][6] = estructura.rm_campo_auxiliar #None # no borrar

					# esto funciona siempre y cuando este por detallado he individual y si esta agrupado no podra realizarce porque entrara varios objetos
					dic_cuentas[cuenta][7] = datos.ids if not tipo_estructura.rm_agrupar_total else [0] # Esto es para enviar el id el objeto en array
		for key,value in dic_cuentas.items():
			if round(value[0],4) == 0.0 and round(value[1],4) == 0.0:
				continue
			if not value[4]:
				detalle.append(self.detalle_comprobante(value,None
						,value[5],int(key),value[6], value[7], fecha_comprobante,nombre_objeto,tipo_estructura))

		glosa = (tipo_estructura.rm_concepto).replace('_fecha_object',self.fecha_literal(fecha_comprobante))
		if not tipo_estructura.rm_agrupar_total:
			dic_glosa = self.env['mail.template']._render_template(glosa,nombre_objeto,objeto.ids,False)
			for key,value in dic_glosa.items():
				glosa = str(value.encode("utf-8")).title()	
		if not detalle:
			return [None]
		vals = {
			'journal_id' : tipo_estructura.rm_serie.id,
			'date' : fecha_comprobante,
			'ref' : glosa,
			'line_ids' : detalle,
		}
		id_move = self.env['account.move'].create(vals)
		
		return id_move.ids

	# FUNCION PARA LINEAS DE DETALLE DE COMPROBANTE
	def detalle_comprobante(self,datos,id_cliente,glosa,account,auxiliar,objeto_id,fecha_comprobante,nombre_objeto,tipo_estructura):
		# _fecha_object esto es para reemplazar la fecha a formato: "22 de abril del 2017"
		glosa = glosa.replace('_fecha_object',self.fecha_literal(fecha_comprobante))
		dic_glosa = self.env['mail.template']._render_template(glosa,nombre_objeto,objeto_id,False)

		for key,value in dic_glosa.items():
			glosa = str(value.encode("utf-8")).title()

		dic_glosa_aux = self.env['mail.template']._render_template(auxiliar,nombre_objeto,objeto_id,False)
		auxiliar_cont = auxiliar
		for key,value in dic_glosa_aux.items():
			auxiliar_cont = str(value.encode("utf-8"))

		# Para la busqueda del auxiliar
		aux = None
		if auxiliar not in [None,False,'']:
			auxiliar_obj = self.env['ew.auxiliar.contable'].search([('name','=',auxiliar_cont)])
			if auxiliar_obj: aux = auxiliar_obj.id
			else: 
				# si no existe lo crea
				auxiliar_obj = self.env['ew.auxiliar.contable'].browse().create({'name' : auxiliar_cont})
				aux = auxiliar_obj.id
		vals_line = {
				'partner_id' : id_cliente,
				'account_id': account,
				'cuenta_auxiliar': aux,
				'name': glosa,
				'debit': datos[0],
				'credit': datos[1],
				'debe_aux': datos[2],
				'haber_aux': datos[3],
				'journal_id': tipo_estructura.rm_serie.id,
				'date': fecha_comprobante,
				'orden_detalle' : 0,
			}
		return (0,0,vals_line)

	# Funcion para totalizar el monto por cada transaccion
	def totales_del_objeto(self,valor_principal,valor_auxiliar,datos_obj,datos_aux,tasa):
		moneda_compania = self.env['res.company'].search([],limit= 1).currency_id
		auxiliar = total = 0
		tipo_cambio = 1
		for campo in valor_principal:
			if campo.name in datos_obj._fields:
				total_ini = round(datos_obj.read()[0][campo.name],4)
			else:
				total_ini = 0.0

			if 'pricelist_id' in datos_obj._fields:
				if datos_obj.pricelist_id.currency_id and datos_obj.pricelist_id.currency_id.name != 'BOB':
					total_ini = total_ini * tasa
					tipo_cambio = tasa
			elif 'currency_id' in datos_obj._fields:
				if datos_obj.currency_id and datos_obj.currency_id.name !='BOB':
					total_ini = total_ini * tasa
					tipo_cambio = tasa
			elif moneda_compania.name != 'BOB':
				total_ini = total_ini * tasa
				tipo_cambio = tasa

			total += total_ini
		for campo in valor_auxiliar:
			if campo.name in datos_aux._fields:
				auxiliar_ini = round(datos_aux.read()[0][campo.name],4)
			else:
				auxiliar_ini = 0.0
			if 'pricelist_id' in datos_aux._fields:
				if datos_aux.pricelist_id.currency_id and datos_aux.pricelist_id.currency_id.name != 'BOB':
					auxiliar_ini = (auxiliar_ini or 0.0) * tasa
					tipo_cambio = tasa
			elif 'currency_id' in datos_aux._fields:
				if datos_aux.currency_id and datos_aux.currency_id.name!='BOB':
					auxiliar_ini = (auxiliar_ini or 0.0) * tasa
					tipo_cambio = tasa
			elif moneda_compania.name != 'BOB':
				auxiliar_ini = (auxiliar_ini or 0.0) * tasa
				tipo_cambio = tasa
			auxiliar += auxiliar_ini
		return total,auxiliar,tipo_cambio

	# Convierte el formato fecha a un formato literal. ejmpplo 26, de abril del 2019
	def fecha_literal(self, fecha):
		fecha_compromiso = fecha.split('-')
		mes = fecha_compromiso[1]
		mes_dic = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
		dia = ('0' + fecha_compromiso[2]) if int(fecha_compromiso[2]) < 10 else fecha_compromiso[2]
		literal = dia + ', de ' + mes_dic[int(fecha_compromiso[1])-1] + ' del ' + fecha_compromiso[0]
		return literal

	# TODO
	# Funcion que obtiene la divisa de la moneda de de bolivianos
	# Parametros fecha> para que obtenga la divisa correspondiente a la fecha
	def obtener_tasa(self,date):
		self.env.cr.execute('''
			CREATE OR REPLACE FUNCTION obtener_tasa(fecha DATE, id_divisa INTEGER) RETURNS TABLE (tasa numeric) AS $$
			BEGIN
			RETURN QUERY
			SELECT
			COALESCE(rcr.rate,1)
			FROM res_currency AS rc
			JOIN res_currency_rate AS rcr ON rcr.currency_id = rc.id
			WHERE rc.id = id_divisa
			AND ((rcr.name AT TIME ZONE 'UTC' AT TIME ZONE 'BOT')::timestamp::date) <= fecha
			ORDER BY rcr.name DESC
			LIMIT 1;
			END
			$$ LANGUAGE plpgsql;
		''')
		self.env.cr.execute("SELECT obtener_tasa(%s,62)",([date]))
		divisa = self.env.cr.fetchone()
		divisa  = divisa[0] if divisa else 1.0
		return divisa

	# Funcion realiza un calculo segun el tipo de operacion por cada linea de la estructura
	# http://prntscr.com/jory5r
	def realizar_operaciones(self, total, valor, operador, tipo_operacion, auxiliar, tasa, nombre_objeto,rm_estructura):
		total_neto = total
		if 'Neto' in tipo_operacion :
			if nombre_objeto not in ['sale.order', 'purchase.order','account.invoice']:
				total -=  auxiliar
			# total *=  ((100.0 - auxiliar) / 100.0)
		elif 'Bruto' in tipo_operacion :
			if nombre_objeto in ['sale.order', 'purchase.order','account.invoice']:
				total += auxiliar
		elif 'Descuento' in tipo_operacion:
			# total *=  (auxiliar / 100.0)
			total =  auxiliar
		elif 'Multiplicacion' in tipo_operacion:
			total *=  float((auxiliar) / float(tasa))
		elif 'Suma' in tipo_operacion:
			total +=  float((auxiliar) / float(tasa))
		elif 'Resta' in tipo_operacion:
			total -=  (float((auxiliar) / float(tasa)))
		elif 'Division' in tipo_operacion:
			total /=  float((auxiliar) / float(tasa))
		elif ('Excento' in tipo_operacion) or ('Excento IVA' in tipo_operacion):
			total -=  (float((auxiliar) / float(tasa)))
		elif 'Retencion' in tipo_operacion:
			cien = 100.0
			for r in rm_estructura:
				if r.rm_cuenta_contable:
					cien -= r.rm_haber.rm_valor
			total /=  (cien/100)

		dic = {
			'porcentaje' : total * (valor/100),
			'igu' : total,
			'sum' : total + valor,
			'res' : total - valor,
			'multi' : total * valor,
			'div' : total / (valor or 1),
			'ninguno' : 0
		}
		resultado = dic[operador]
		if 'Excento IVA' in tipo_operacion and resultado !=0:
			resultado += (float((auxiliar) / float(tasa))) #Factura de combustible
		return float(resultado)

	# Busca el cliente en el objeto de res.partner para que pueda entrar como axuliar en el comprobante.
	# http://prntscr.com/jorwo8
	def obtener_cliente(self, partner):
		partner_id = False
		# 'ew_forwarder' esta pendiente de importaciones
		datos = ['partner_id','rm_cliente','ew_proveedor_costo','cliente','ew_cliente_id','ew_cliente','rm_proveedor','rm_partner']
		for x in datos:
			if x in partner._fields:
				persona = partner.read()[0][x]
				if persona:
					return self.env['res.partner'].browse(persona[0])
		return partner_id