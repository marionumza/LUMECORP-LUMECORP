# -*- coding: utf-8 -*-
##############################################################################

from odoo import _, api, fields, models
from odoo import tools
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

import logging
_logger = logging.getLogger(__name__)

class stock_return_picking(models.TransientModel):
	_inherit = 'stock.return.picking'

	rm_estructura = fields.Many2one('ng.estructura.comprobante', string='Estructura contable')