# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import time
import logging
_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.onchange('partner_id')
    def cargar_datos_cliente(self):
        self.razon_social = self.partner_id.razon_social if self.partner_id.razon_social else self.partner_id.name
        self.nit = self.partner_id.nit or '0'

    razon_social = fields.Char(string='Razón Social', required=True, default='S/N')
    nit = fields.Char(string='NIT/CI', required=True, default='0')
    dosificacion = fields.Many2one('dosificacion', string='Dosificación', domain="[('state','=','Activo')]")
    fecha_factura = fields.Date(string='Fecha Factura', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))

    @api.multi
    def _prepare_invoice(self):
        invoice_vals = super(SaleOrder, self)._prepare_invoice()
        invoice_vals['razon_social'] = self.razon_social
        invoice_vals['nit'] = self.nit
        invoice_vals['date_invoice'] = self.fecha_factura
        invoice_vals['tipo_factura'] = self.dosificacion.tipo_documento
        invoice_vals['dosificacion'] = self.dosificacion.id
        return invoice_vals

    def validacion_dosificacion(self, dosificacion, fecha):
        if self.dosificacion:
            if fecha > dosificacion.fecha_limite:
                raise models.ValidationError('La Fecha de Factura (%s) es Mayor a la Fecha Límite de Emisión de la Dosificación Seleccionada.')

    @api.multi
    def confirmar_facturar(self):
        if not self.dosificacion:
            raise models.ValidationError('Seleccione una Dosificación para Crear la Factura.')
        super(SaleOrder, self).action_confirm()
        for sale in self:
            self.validacion_dosificacion(self.dosificacion,self.fecha_factura)
            return self.ver_factura(sale.action_invoice_create())


    @api.multi
    def action_confirm(self):
        result = super(SaleOrder, self).action_confirm()
        for sale in self:
            sale.validacion_dosificacion(self.dosificacion,self.fecha_factura)
        return result

    @api.multi
    def crear_facturas(self):
        for sale in self:
            if not sale.dosificacion:
                raise models.ValidationError('Seleccione una Dosificación para Crear la Factura.')
            return self.ver_factura(sale.action_invoice_create())

    def ver_factura(self,invoice_ids):
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoice_ids) > 1:
            action['domain'] = [('id', 'in', invoice_ids)]
        elif len(invoice_ids) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoice_ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action