# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging

_logger = logging.getLogger(__name__)
class ProductTemplate(models.Model):
    _inherit = 'product.template'

    type = fields.Selection(default='product')

    @api.model
    def create(self, vals):
        list_ids_taxes = self.env['account.tax'].search([('type_tax_use','=','sale'),('active','=',True),('company_id','=',vals.get('company_id'))]).ids
        vals['taxes_id'] = [[6, False, list_ids_taxes]]
        result = super(ProductTemplate, self).create(vals)
        return result