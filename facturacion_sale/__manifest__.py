# -*- coding: utf-8 -*-

{
    'name': "Facturación Sales",
    'summary': """
        Facturación Computarizada para el módulo de Ventas (Sales).
    """,
    'description': """
        Módulo que permite la facturación computarizada bajo normativa boliviana.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'codigo_control',
        'sale',
        'sale_management',
        'stock'
    ],
    'data': [
        'views/sale_order_view.xml',
    ],
    'application': True,
}