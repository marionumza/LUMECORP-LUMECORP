# -*- coding: utf-8 -*-
#################################################################################
##    Copyright (c) 2018-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#    You should have received a copy of the License along with this program.
#    If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Product Variant Extra Price",
  "summary"              :  "This module allows you to manually apply additional extra prices for Product's variants.",
  "category"             :  "Uncategorized",
  "version"              :  "1.1.0",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "license"              :  "Other proprietary",
  "website"              :  "https://store.webkul.com/Odoo-Product-Variant-Extra-Price.html",
  "description"          :  """http://webkul.com/blog/product-variant-extra-price-different-weight/""",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=variant_price_extra",
  "depends"              :  ['product'],
  "data"                 :  [
                             'views/product_inherit_view.xml',
                            ],
  'demo': [
      'demo/variant_demo_data.xml',
  ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  20,
  "currency"             :  "EUR",
  "pre_init_hook"        :  "pre_init_check",
}
