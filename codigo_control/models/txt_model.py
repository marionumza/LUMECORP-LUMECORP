# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)
class TxtExtended(models.Model):
    _name= 'txt.extended'
    _description = 'Modelo base para obtener formatos txt'

    txt_file = fields.Binary('Descargar Archivo')
    file_name = fields.Char('Txt File')