# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)
class XlsExtended(models.Model):
    _name= 'xls.extended'
    _description = 'Modelo base para reportes en formato XLS'

    excel_file = fields.Binary('Descargar Archivo')
    file_name = fields.Char('XLS File')