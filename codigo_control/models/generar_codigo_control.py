# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime
from . import codigo_control as cc
# from odoo.exceptions IMPORT UserError
# from odoo.osv import orm, osv
import logging
_logger = logging.getLogger(__name__)

class GenerarCodigoControl(models.Model):
    _name = 'generar.codigo.control'
    _description = 'Modelo para generación de codigos de control basados en ciertos parámetros'

    def validar_datos(self, linea):
        errores = []
        if linea.numero_autorizacion:
            try: int(linea.numero_autorizacion)
            except ValueError: errores.append("» Número de Autorización tiene que ser un valor Entero.")
        else: errores.append("» Ingrese el Número de Autorización en la linea.")
        if linea.numero_factura:
            try: int(linea.numero_factura)
            except ValueError: errores.append("» Número de Factura tiene que ser un valor Entero.")
        else: errores.append("» Ingrese el Número de Factura en la linea.")
        if linea.nit_cliente:
            try: int(linea.nit_cliente)
            except ValueError: errores.append("» El NIT tiene que ser un valor Entero.")
        else: errores.append("» Ingrese el NIT en la linea.")
        return errores

    @api.multi
    def generar_codigo_control(self):
        errores_finales = []
        linea = 0
        for generar in self:
            linea += 1
            errores = self.validar_datos(generar)
            if len(errores) == 0:
                generar.codigo_control = cc.generar_codigo_control(str(generar.numero_autorizacion), int(generar.numero_factura), int(generar.nit_cliente), datetime.strptime(str(generar.fecha_factura), "%Y-%m-%d").strftime("%Y%m%d"), str(generar.monto_factura), str(generar.llave_dosificacion))
            else:
                errores_finales.append("\nLa linea %s tiene las siguientes observaciones:\n%s" % (linea, "\n".join(errores)))
        if len(errores_finales) > 0:
            raise models.ValidationError("Existe(n) esta(s) observación(es):\n%s" % "\n".join(errores_finales))

    numero_autorizacion = fields.Char(string='N° de Autorización')
    numero_factura = fields.Char(string='N° de Factura')
    nit_cliente = fields.Char(string='NIT Cliente')
    fecha_factura = fields.Date(string='Fecha Factura', required=True)
    monto_factura = fields.Float(string='Monto Factura')
    llave_dosificacion = fields.Char(string='Llave de Dosificación')
    codigo_control = fields.Char(string='Código de Control')