# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import time
import logging
_logger = logging.getLogger(__name__)

class Dosificacion(models.Model):
    _name = 'dosificacion'
    _description = 'Modelo para registro de las dosificaciones para la generación de facturas computarizadas'
    _rec_name = 'numero_autorizacion'

    state = fields.Selection([('Activo','Activado'),('Inactivo','Inactivo')], string='Estado', default='Inactivo')
    empresa_unipersonal = fields.Boolean(string='Unipersonal')
    tipo_papel = fields.Selection([('Ancho','Ancho'),('Rollo','Rollo')], string='Tipo de Papel', required=True, default='Rollo')
    nombre_unipersonal = fields.Char(string='Nombre Unipersonal')
    numeracion_actual = fields.Integer(string='N° Actual de Factura', default=1)
    logo_empresa = fields.Boolean(string='Logo')
    imagen_logo = fields.Binary(string="Imagen", attachment=True)
    tipo_documento = fields.Selection([('Computarizado','Computarizado'),('Talonario','Talonario')], string='Documento', default='Computarizado', required=True)

    company_id = fields.Many2one('res.company', string='Compañía', default=lambda self: self.env.user.company_id)
    razon_social_empresa = fields.Char(string='Razón Social Empresa', required=True)
    nit_empresa = fields.Char(string='NIT Empresa', required=True)
    actividad_economica = fields.Char(string='Actividad Económica', required=True)
    ciudad = fields.Many2one('res.country.state', string='Ciudad', required=True)
    municipio = fields.Char(string='Municipio')
    sucursal = fields.Selection(
        [('Casa Matriz', 'Casa Matriz'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8'),
         ('9', '9'), ('10', '10'), ('11', '11'), ('12', '12'), ('13', '13'), ('14', '14'), ('15', '15')],
        string='Sucursal', required=True, states={'Inactivo': [('readonly', False)]})
    direccion1 = fields.Char(string='Dirección 1', required=True)
    direccion2 = fields.Char(string='Dirección 2')
    direccion3 = fields.Char(string='Dirección 3')
    telefono = fields.Char(string='Teléfono(s)')
    celular = fields.Char(string='Celular(es)')

    fecha_limite = fields.Date(string='Fecha límite Emisión', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    numero_autorizacion = fields.Char(string='N° de Autorización', required=True)
    llave_dosificacion = fields.Char(string='Llave de Dosificación')
    leyenda1 = fields.Char(string='Leyenda 1', required=True)
    leyenda2 = fields.Char(string='Leyenda 2', required=True)
    responsable_legal = fields.Char(string='Responsable Legal')
    ci_responsable_legal = fields.Char(string='C.I. Responsable Legal')

    def action_activar(self):
        self.state = 'Activo'

    def action_desactivar(self):
        self.state = 'Inactivo'

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = record.company_id.name + ' - ' + str(record.fecha_limite) + '(' + record.tipo_documento + ')'
            result.append((record.id, name))
        return result

    @api.model
    def nombre_tipo_papel(self, id_dosificacion):
        return {'nombre':self.env['dosificacion'].browse(id_dosificacion).tipo_papel}