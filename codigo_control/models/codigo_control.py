# -*- coding: utf-8 -*-
from . import verhoeff
from . import base64
import binascii
import re
from Crypto.Cipher import ARC4

def generar_codigo_control(numero_autorizacion, numero_factura, nit, fecha_factura, monto_total, llave_dosificacion):
    # Paso 1
    var_numero_factura = verhoeff.generate(numero_factura, 2)
    var_nit = verhoeff.generate(nit, 2)
    var_fecha_factura = verhoeff.generate(fecha_factura, 2)
    var_monto_total = verhoeff.generate(int(round(float(monto_total.replace(',', '.')))), 2)
    var_sum = int(var_numero_factura) + int(var_nit) + int(var_fecha_factura) + int(var_monto_total)
    ver_var_sum = verhoeff.generate(var_sum, 5)

    # Paso 2
    validar_ver = list(ver_var_sum[len(ver_var_sum) - 5:len(ver_var_sum)])
    lista_ver = validar_ver[:]
    lista_datos = [None] * len(lista_ver)

    i = 0
    while i < len(lista_ver):
        lista_ver[i] = (int(lista_ver[i]) + 1)
        if i > 0:
            lista_ver[i] = lista_ver[i] + lista_ver[i - 1]
            lista_datos[i] = llave_dosificacion[lista_ver[i - 1]:lista_ver[i]]
        else:
            lista_datos[i] = llave_dosificacion[0:lista_ver[i]]
        i = i + 1

    lista_datos[0] = numero_autorizacion + lista_datos[0]
    lista_datos[1] = var_numero_factura + lista_datos[1]
    lista_datos[2] = var_nit + lista_datos[2]
    lista_datos[3] = var_fecha_factura + lista_datos[3]
    lista_datos[4] = var_monto_total + lista_datos[4]

    # Paso 3
    cadena_llave_dosificacion = llave_dosificacion + ver_var_sum[len(ver_var_sum) - 5:len(ver_var_sum)]
    cifrar = ARC4.new(cadena_llave_dosificacion)
    ps = ''.join(lista_datos)
    cifrar_text = binascii.hexlify(cifrar.encrypt(ps)).upper()

    # Paso 4
    codes = [ord(c) for c in cifrar_text.decode("utf-8")]
    index = 0
    lista_codigos = [0] * 5
    for code in codes:
        for k in range(0, 5):
            if ((index - k) % 5) == 0:
                lista_codigos[k] += code
        index += 1
    suma_codigos = sum(lista_codigos)

    # Paso 5
    i = 0
    m = [None] * len(lista_codigos)
    while i < len(lista_codigos):
        m[i] = (suma_codigos * lista_codigos[i]) // (int(validar_ver[i]) + 1)
        i += 1
    bs = base64.b64(sum(m))

    # Paso 6
    fcifrar = ARC4.new(cadena_llave_dosificacion)
    control_code = binascii.hexlify(fcifrar.encrypt(bs)).upper()
    control_code = '-'.join(re.findall('.{1,2}', control_code.decode("utf-8")))

    return control_code