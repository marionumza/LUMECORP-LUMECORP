from . import txt_model
from . import xls_model
from . import res_partner
from . import generar_codigo_control
from . import dosificacion
from . import detalle_libro_venta
from . import account
from . import account_invoice