# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from datetime import datetime
from . import codigo_control as cc
from . import convertir_numero_texto as convertir_texto
import re
import logging
_logger = logging.getLogger(__name__)

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def write(self, vals):
        detalle_obj = self.env['account.invoice'].browse(self.id).detalle_ventas_id
        if detalle_obj:
            detalle_libro_ventas = {}

            if vals.get('date_invoice'): detalle_libro_ventas['fecha_factura'] = datetime.strptime(vals.get('date_invoice'), "%Y-%m-%d").strftime("%Y-%m-%d")
            if vals.get('numero_factura'): detalle_libro_ventas['numero_factura'] = vals.get('numero_factura')
            if vals.get('numero_autorizacion'): detalle_libro_ventas['numero_autorizacion'] = vals.get('numero_autorizacion')
            if vals.get('estado_factura'): detalle_libro_ventas['estado_factura'] = vals.get('estado_factura')
            if vals.get('nit'): detalle_libro_ventas['nit_factura'] = vals.get('nit')
            if vals.get('razon_social'): detalle_libro_ventas['razon_social_factura'] = vals.get('razon_social')
            if vals.get('amount_total'):
                detalle_libro_ventas['importe_total'] = vals.get('amount_total')
                detalle_libro_ventas['sub_total'] = vals.get('amount_total')
            if vals.get('codigo_control'): detalle_libro_ventas['codigo_control'] = vals.get('codigo_control')

            if detalle_libro_ventas: detalle_obj.write(detalle_libro_ventas)
        return super(AccountInvoice, self).write(vals)

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        super(AccountInvoice, self)._onchange_partner_id()
        self.razon_social = self.partner_id.razon_social if self.partner_id.razon_social else self.partner_id.name
        self.nit = self.partner_id.nit if self.partner_id.nit else 0
        self.date_invoice = self._context.get('date_invoice') if 'date_invoice' in self._context else str(datetime.now())

    @api.multi
    def numero_factura_entero(self):
        for invoice in self:
            try:
                invoice.numero_factura_tree = int(invoice.numero_factura)
            except ValueError:
                raise UserError(_("El número de factura debe ser un número entero"))

    @api.onchange('tipo_factura')
    def cambio_tipo_factura(self):
        self.dosificacion = self.env['dosificacion'].search([('tipo_documento','=',self.tipo_factura)],limit=1)

    @api.onchange('dosificacion')
    def cambio_dosificacion(self):
        self.numero_factura = self.dosificacion.numeracion_actual
        self.numero_autorizacion = self.dosificacion.numero_autorizacion

    @api.onchange('codigo_control')
    def cambio_codigo_control(self):
        if self.codigo_control:
            codigo_nuevo = self.codigo_control.replace('-', '')
            cadena = '-'.join(re.findall('.{1,2}', codigo_nuevo))
            self.codigo_control = cadena.upper()

    @api.depends('nit', 'numero_factura', 'date_invoice', 'amount_total', 'codigo_control')
    def _actualizar_qr(self):
        for invoice in self:
            if invoice.numero_factura or invoice.numero_factura > 0:
                qr = str(invoice.dosificacion.nit_empresa or '0') + '|'                                                 # NIT emisor
                qr = qr + str(invoice.numero_factura or '0') + '|'                                                      # Numero factura
                qr = qr + str(invoice.dosificacion.numero_autorizacion or '0') + '|'                                    # Numero autorizacion
                qr = qr + str(datetime.strptime(str(invoice.date_invoice), "%Y-%m-%d").strftime("%d/%m/%Y") or '0') + '|'    # fecha de emision (DD/MM/AAAA)
                qr = qr + str(format(invoice.amount_total, '.2f') or '0') + '|'                                         # Total
                qr = qr + str(format(invoice.amount_total, '.2f') or '0') + '|'                                         # Importe credito fiscal
                qr = qr + str(invoice.codigo_control or '0') + '|'                                                      # Codigo de control
                qr = qr + str(invoice.nit or '0') + '|'                                                                 # NIT/CI/CEX comprador
                qr = qr + str(0) + '|'                                                                                  # Importe ICE /IEHD /TASAS
                qr = qr + str(0) + '|'                                                                                  # Importe por ventas gravadas o Gravadas a tasa cero
                qr = qr + str(0) + '|'                                                                                  # Importe no sujeto a credito fiscal
                qr = qr + str(0)                                                                                        # Descuentos, bonificaciones y rebajas obtenidas
                invoice.codigo_qr_factura = qr

    razon_social = fields.Char(string='Razón Social')
    nit = fields.Char(string='NIT')
    numero_factura = fields.Char(string='N° de Factura', copy=False)
    numero_factura_tree = fields.Integer(string='N° de Factura', copy=False, compute='numero_factura_entero')
    numero_autorizacion = fields.Char(string='N° de Autorización')
    codigo_control = fields.Char(string='Código de Control')
    estado_factura = fields.Selection([('A', 'ANULADA'), ('V', 'VALIDA'), ('E', 'EXTRAVIADA'), ('N', 'NO UTILIZADA'),('C', 'EMITIDA POR CONTINGENCIA'), ('L', 'LIBRE CONSIGNACIÓN')], string='Estado Factura', default='V')
    motivo_anulacion = fields.Char(string='Motivo de Anulación')
    codigo_qr_factura = fields.Char(string='Código QR', compute='_actualizar_qr', store=True)
    dosificacion = fields.Many2one('dosificacion', string='Dosificación')
    numero_impresion = fields.Integer(string='N° de Impresión', default=0)
    tipo_factura = fields.Selection([('Computarizado','Computarizado'),('Talonario','Talonario')], string='Tipo factura', default='Talonario')
    detalle_ventas_id = fields.Many2one('detalle.libro.venta')

    tipo_compra = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')],
                                   string='Tipo de Compra', required=True, default='1',
                                   help=_('1 = Compras para mercado interno con destino a actividades gravadas,\n'
                                          '2 = Compras para mercado interno con destino a actividades no gravadas,\n'
                                          '3 = Compras sujetas a proporcionalidad,\n'
                                          '4 = Compras para exportaciones,\n'
                                          '5 = Compras tanto para el mercado interno como para exportaciones.'))
    numero_dui = fields.Char(string='Número de DUI', default=0)
    ice_iehd = fields.Float(string='ICE/IEHD')
    exento = fields.Float(string='Exento')
    metodo_descuento = fields.Selection([('Fijo', 'Fijo'), ('Porcentaje', 'Porcentaje')], string='Método Descuento', default='Fijo', required=True)
    descuento = fields.Float(string='Monto Descuento', help='Descuento aplicado de acuerdo al metodo')
    tipo_factura_consumo = fields.Selection([('Normal', 'Normal'), ('Combustible', 'Combustible'), ('Servicios Básicos', 'Servicios Básicos')], string='Tipo Factura', default='Normal')

    _sql_constraints = [('numero_factura_numero_autorizacion_uniq', 'unique (numero_factura,numero_autorizacion)',
                         'No se Puede Tener Facturas Duplicadas, Revise que el N° de Autorización No Tenga el Mismo N° de Factura')]

    def resetear_numero_impresion(self):
        self.numero_impresion = 0

    def numero_texto(self, monto_total, moneda):
        tmp = str(monto_total).split('.')
        entero = monto_total
        decimales = '0'
        if len(tmp) > 1:
            entero = int(tmp[0])
            decimales = tmp[1]

        texto_final = convertir_texto.integer_to_word(entero)
        texto_final += convertir_texto.decimal_number_to_text(decimales)
        texto_final += str(moneda) if moneda else 'Bs.'
        return (texto_final[:1].upper() + texto_final[1:])

    @api.multi
    def imprimir_factura_qr(self):
        data = {}
        self.numero_impresion = self.numero_impresion + 1
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['account_id', 'dosificacion', 'numero_impresion'])[0]
        data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
        data['form']['invoice_ids'] = self.env['account.invoice'].browse([self.id,self.id]).ids
        data['copia'] = True

        if self.dosificacion.tipo_papel == 'Ancho':
            return self.env.ref('codigo_control.report_factura_computarizada_hoja_carta').report_action(self, data=data)
        elif self.dosificacion.tipo_papel == 'Rollo':
            return self.env.ref('codigo_control.report_factura_computarizada_rollo').report_action(self, data=data)

    def validar_dosificacion(self, dosificacion, fecha):
        if not self.dosificacion:
            raise Warning(_("No se ha seleccionado dosificación, Seleccione una dosificación para generar la factura."))
        if fecha > dosificacion.fecha_limite:
            raise models.ValidationError(_('La fecha de factura es mayor a la fecha límite de emisión de su dosificación.'))

    def cargar_factura_libro_ventas(self, invoice):
        detalle_libro_ventas = {
            'especificacion': 3,
            'numero_correlativo': 0,
            'fecha_factura': datetime.strptime(str(invoice.date_invoice), "%Y-%m-%d").strftime("%Y-%m-%d"),
            'numero_factura': invoice.numero_factura,
            'numero_autorizacion': invoice.numero_autorizacion,
            'estado_factura': 'V',
            'nit_factura': invoice.nit,
            'razon_social_factura': invoice.razon_social,
            'importe_total': invoice.amount_total,
            'importe_ice': 0,
            'operaciones_excentas': 0,
            'ventas_gravadas': 0,
            'sub_total': invoice.amount_total,
            'descuento': 0,
            'importe_total_base': invoice.amount_total - 0,
            'debito_fiscal': invoice.amount_total * 0.13,
            'codigo_control': invoice.codigo_control,
            'invoice_id': invoice.id,
        }
        if not invoice.detalle_ventas_id and invoice.state != 'draft':
            detalle_id = self.env['detalle.libro.venta'].create(detalle_libro_ventas)
            invoice.detalle_ventas_id = detalle_id
        else:
            detalle_libro_ventas['invoice_id'] = invoice.detalle_ventas_id.invoice_id.id
            invoice.detalle_ventas_id.write(detalle_libro_ventas)

    @api.multi
    def action_invoice_open(self):
        result = super(AccountInvoice, self).action_invoice_open()
        if self.type == 'out_invoice' and self.tipo_factura == 'Computarizado':
            self.validar_dosificacion(self.dosificacion,self.date_invoice)
            dosificacion = self.dosificacion
            numero_factura_actual = self.dosificacion.numeracion_actual
            vals = {
                'numero_factura': str(numero_factura_actual),
                'codigo_control': cc.generar_codigo_control(dosificacion.numero_autorizacion, numero_factura_actual, int(self.nit), datetime.strptime(str(self.date_invoice), "%Y-%m-%d").strftime("%Y%m%d"), str(self.amount_total), str(dosificacion.llave_dosificacion)),
                'numero_autorizacion': dosificacion.numero_autorizacion,
                'estado_factura': 'V',
            }
            self.write(vals)
            dosificacion.write({'numeracion_actual':numero_factura_actual+1})

        partner = self.env['res.partner'].browse(self.partner_id.id)
        if not self.partner_id.razon_social: partner.write({'razon_social':self.razon_social})
        if not self.partner_id.nit: partner.write({'nit':self.nit})
        self.cargar_factura_libro_ventas(self)
        return result