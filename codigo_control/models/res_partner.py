# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    EXPEDIDO = [
        ('CB', 'CB'),('LP', 'LP'),('SC', 'SC'),
        ('OR', 'OR'),('TJ', 'TJ'),('BE', 'BE'),
        ('PA', 'PA'),('PO', 'PO'),('CH', 'CH'),
    ]

    @api.onchange('name')
    def cargar_razon_social(self):
        self.razon_social = self.name

    @api.onchange('nit')
    def cargar_ci(self):
        self.ci = self.nit

    razon_social = fields.Char(string='Razón Social', default='S/N')
    nit = fields.Char(string='NIT', default='0')
    ci = fields.Char(string='C.I.', default='0')
    expedido = fields.Selection(EXPEDIDO, string='Expedido')
    unipersonal = fields.Char(string='Nombre Unipersonal', help='Información para uso en la cabecera de reportes')

    tz = fields.Selection(default='America/La_Paz')