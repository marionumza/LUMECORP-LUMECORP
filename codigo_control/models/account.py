# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)

class AccountAccount(models.Model):
    _inherit = 'account.account'

    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for account in self:
            name = account.code + ' ' + account.name + ' (' + account.company_id.name + ')'
            result.append((account.id, name))
        return result

class AccountJournal(models.Model):
    _inherit = 'account.journal'

    @api.multi
    @api.depends('name', 'currency_id', 'company_id', 'company_id.currency_id')
    def name_get(self):
        res = []
        for journal in self:
            currency = journal.currency_id or journal.company_id.currency_id
            name = "%s (%s - %s)" % (journal.name, journal.company_id.name, currency.name)
            res += [(journal.id, name)]
        return res

class AccountTax(models.Model):
    _inherit = 'account.tax'

    retencion = fields.Boolean(string='Retención', default=False)