# -*- coding: utf-8 -*-

import textwrap
import itertools

def integer_to_word(number):
    hundreds = map(string_reverse_to_int, textwrap.wrap(str(int(number))[::-1], 3))
    millions = [
        {True: None, False: None},
        {True: 'millón', False: 'millones'},
        {True: 'billón', False: 'billones'},
        {True: 'trillón', False: 'trillones'}
    ]
    last_hundred = 0
    count = -1
    words = []

    for numbers in hundreds:
        count += 1
        if count%2 == 0:
            last_hundred = numbers
            if numbers > 0:
                aux = [hundred_to_word(numbers), millions[count//2][numbers==1]]
                words.append([x for x in aux if x is not None])
        elif last_hundred == 0:
            if numbers > 0:
                aux = [hundred_to_word(numbers), 'mil', millions[count//2][False]]
                words.append([x for x in aux if x is not None])
        else:
            if numbers > 0:
                words.append([hundred_to_word(numbers), 'mil'])

    words = [x for x in words if x is not None][::-1]
    return ' '.join(list(itertools.chain.from_iterable(words)))

def decimal_number_to_text(decimal):
    numbers = list(map(int, textwrap.wrap(str(decimal), 1)))
    literal = ' '
    if len(numbers) > 1:
        literal += str(numbers[0])
        literal += str(numbers[1])
    else:
        literal += str(numbers[0])
        literal += '0'
    literal += '/100 '
    return literal

def string_reverse_to_int(x):
    return int(x[::-1])

def hundred_to_word(number):
    specials = {
        11: 'once',
        12: 'doce',
        13: 'trece',
        14: 'catorce',
        15: 'quince',
        10: 'diéz',
        20: 'veinte',
        21: 'veintiún',
        100: 'cien'
    }

    if number in specials:
        return specials[number]

    hundreds = [
        None,
        'ciento',
        'doscientos',
        'trescientos',
        'cuatrocientos',
        'quinientos',
        'seiscientos',
        'setecientos',
        'ochocientos',
        'novecientos'
    ]

    tens = [
        None,
        'dieci',
        'veinti',
        'treinta',
        'cuarenta',
        'cincuenta',
        'sesenta',
        'setenta',
        'ochenta',
        'noventa'
    ]

    units = [
        None,
        'un',
        'dos',
        'tres',
        'cuatro',
        'cinco',
        'seis',
        'siete',
        'ocho',
        'nueve'
    ]

    hundred, ten, unit = map(int, textwrap.wrap(str(number).rjust(3, '0'), 1))
    words = []
    words.append(hundreds[hundred])
    if (ten * 10 + unit) in specials:
        words.append(specials[(ten * 10 + unit)])
    else:
        tmp = ''
        if tens[ten] is not None:
            tmp = str(tens[ten])
        if ten > 2 and unit > 0:
            tmp += ' y '
        if units[unit] is not None:
            tmp += str(units[unit])
        if not tmp:
            words.append(None)
        else:
            words.append(tmp)

    return ' '.join([x for x in words if x is not None])