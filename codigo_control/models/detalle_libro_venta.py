# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)

class DetalleLibroVenta(models.Model):
    _name = 'detalle.libro.venta'
    _description = 'Modelo para el registro de libro de ventas manuales, o generados por el modelo de account.invoice'
    _rec_name = 'numero_factura'
    _order = 'numero_autorizacion, numero_factura'

    @api.onchange('importe_total', 'importe_ice', 'operaciones_excentas', 'ventas_gravadas')
    def _compute_sub_total(self):
        self.sub_total = self.importe_total - self.importe_ice - self.operaciones_excentas - self.ventas_gravadas

    @api.onchange('sub_total','descuento','importe_total_base')
    def _compute_importe_total_base_debito_fiscal(self):
        self.importe_total_base = self.sub_total - self.descuento
        self.debito_fiscal = float(self.importe_total_base * 0.13)

    def validacion_datos(self,valores):
        errores = []
        if 'numero_factura' in valores:
            try: int(valores.get('numero_factura'))
            except ValueError: errores.append(_("» Número de Factura tiene que ser un valor Entero."))
        if 'numero_autorizacion' in valores:
            try: int(valores.get('numero_autorizacion'))
            except ValueError: errores.append(_("» Número de Autorización tiene que ser un valor Entero."))
        if 'nit_factura' in valores:
            try: int(valores.get('nit_factura'))
            except ValueError: errores.append(_("» El NIT tiene que ser un valor Entero."))
        return errores

    @api.model
    def create(self, valores):
        resultado_validacion = self.validacion_datos(valores)
        if len(resultado_validacion) > 0:
            raise models.ValidationError(("Existen los siguientes errores:\n%s") % "\n".join(resultado_validacion))
        datos = super(DetalleLibroVenta, self).create(valores)
        return datos

    @api.multi
    def write(self, valores):
        resultado_validacion = self.validacion_datos(valores)
        if len(resultado_validacion) > 0:
            raise models.ValidationError(("Existen los siguientes errores:\n%s") % "\n".join(resultado_validacion))
        datos = super(DetalleLibroVenta, self).write(valores)
        return datos

    especificacion = fields.Integer(string='Especificación')
    numero_correlativo = fields.Integer(string='N° Correlativo')
    fecha_factura = fields.Date(string='Fecha de la Factura', required=True)
    numero_factura = fields.Char(string='N° de Factura')
    numero_autorizacion = fields.Char(string='N° de Autorización')
    estado_factura = fields.Selection([('A', 'ANULADA'), ('V', 'VALIDA'), ('E', 'EXTRAVIADA'), ('N', 'NO UTILIZADA'),('C', 'EMITIDA POR CONTINGENCIA'), ('L', 'LIBRE CONSIGNACIÓN')], string='Estado Factura', default='V', required=True)
    nit_factura = fields.Char(string='NIT/CI Cliente')
    razon_social_factura = fields.Char(string='Razón Social')
    importe_total = fields.Float(string='Imp. Total Venta A')
    importe_ice = fields.Float(string='Imp. ICE/ IEHD/ Tasas B')
    operaciones_excentas = fields.Float(string='Export. y Oper. Exent. C')
    ventas_gravadas = fields.Float(string='Ventas Grabadas D')
    sub_total = fields.Float(string='SubTotal E = A - B - C - D')
    descuento = fields.Float(string='Desc., Bon. y Rebajas F')
    importe_total_base = fields.Float(string='Imp. Base p/ Débito Fiscal G = E - F')
    debito_fiscal = fields.Float(string='Débito Fiscal H = G * 13%')
    codigo_control = fields.Char(string='Código de Control')
    cuenta_contable = fields.Many2one('account.account', string='Cuenta Contable')
    invoice_id = fields.Many2one('account.invoice')

    def abrir_importacion(self):
        if self.invoice_id:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'account.invoice',
                'res_id': self.invoice_id.id,
                'views': [[self.env.ref('account.invoice_form', False).id, 'form']]
            }