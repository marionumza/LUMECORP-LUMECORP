# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class LibroVentas(models.AbstractModel):
    _name = 'report.codigo_control.libro_ventas'
    _description = 'Reporte de libro de ventas'

    def _detalle(self, data):
        numeros_autorizacion = [x.numero_autorizacion for x in self.env['lista.dosificacion'].search([('id', 'in', data['form']['dosificacion'])])]
        numeros_autorizacion = str("('"+numeros_autorizacion[0]+"')") if len(numeros_autorizacion) == 1 else tuple(numeros_autorizacion)
        consulta = """
        SELECT 
        to_char(dlv.fecha_factura,'DD/MM/YYYY') as fecha
        ,dlv.numero_factura::INTEGER as numero_factura
        ,dlv.numero_autorizacion as autorizacion
        ,dlv.estado_factura as estado
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.nit_factura ELSE '0' END) as nit
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.razon_social_factura ELSE 'anulado' END) as razon
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.importe_total  ELSE 0 END) as importe_total
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.importe_ice ELSE 0 END) as ice
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.operaciones_excentas ELSE 0 END) as excentas
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.ventas_gravadas ELSE 0 END) as gravadas
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.sub_total ELSE 0 END) as sub_total
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.descuento ELSE 0 END) as descuento
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.importe_total_base ELSE 0 END) as importe_base
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.debito_fiscal ELSE 0 END) as debito
        ,dlv.codigo_control as control
        ,dlv.fecha_factura AS fecha_original
        FROM detalle_libro_venta dlv
        WHERE dlv.numero_factura NOTNULL AND dlv.numero_autorizacion NOTNULL AND nit_factura NOTNULL AND razon_social_factura NOTNULL """ + \
        "AND dlv.fecha_factura BETWEEN '" + str(data['form']['fecha_inicial']) + "' AND '" + str(data['form']['fecha_final']) +"'" \
        "AND dlv.numero_autorizacion IN " + str(numeros_autorizacion) + " " \
        " ORDER BY autorizacion,fecha_original,numero_factura"

        self.env.cr.execute(consulta)
        resultado_consulta = [i for i in self.env.cr.fetchall()]

        nro = 0
        detalle_factura = []
        for consulta in resultado_consulta:
            nro += 1
            dic = {
                'nro'                   : nro, # 2. Número
                'fecha_factura'         : consulta[0], # 3. Fecha de la Factura
                'numero_factura'        : consulta[1], # 4. Número de Factura
                'numero_autorizacion'   : consulta[2], # 5. Número de Autorización
                'estado_factura'        : consulta[3], # 6. Estado
                'ci_nit_cliente'        : consulta[4], # 7. NIT / CI Cliente
                'razon_social'          : consulta[5], # 8. Razón Social
                'importe_total_a'       : consulta[6], # 9. Importe total de la venta
                'importe_ice_b'         : consulta[7], # 10. Importe ICE/IEHD/IPJ/TASAS/OTROS NO SUJETOS AL IVA
                'export_opera_exentas_c': consulta[8], # 11. Exportaciones y Operaciones Exentas
                'ventas_gravadas_d'     : consulta[9], # 12. Ventas Gravadas a Tasa Cero
                'subtotal_e'            : consulta[10], # 13. Subtotal
                'rebajas_f'             : consulta[11], # 14. Descuentos, Bonificaciones y Rebajas Sujetas al IVA
                'importe_base_g'        : consulta[12], # 15. Importe Base Para Débito Fiscal
                'debito_fiscal_h'       : consulta[13], # 16. Debito Fiscal
                'codigo_control'        : consulta[14] or 0, # 17. Código de Control
            }
            detalle_factura.append(dic)
        return detalle_factura

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))
        total_importe_total_a = total_importe_ice_b = total_export_opera_exentas_c = total_ventas_gravadas_d = total_subtotal_e = total_rebajas_f = total_importe_base_g = total_debito_fiscal_h = 0
        for register in self._detalle(data):
            total_importe_total_a += register['importe_total_a']
            total_importe_ice_b += register['importe_ice_b']
            total_export_opera_exentas_c += register['export_opera_exentas_c']
            total_ventas_gravadas_d += register['ventas_gravadas_d']
            total_subtotal_e += register['subtotal_e']
            total_rebajas_f += register['rebajas_f']
            total_importe_base_g += register['importe_base_g']
            total_debito_fiscal_h += register['debito_fiscal_h']

        if data['form']['dosificacion']!=False:
            for lista in self.env['lista.dosificacion'].search([('id', 'in', data['form']['dosificacion'])]):
                dosificacion = self.env['dosificacion'].search([('numero_autorizacion', '=', lista.numero_autorizacion)],limit=1)
                if dosificacion: break

        return {
            'doc_ids': data['form']['id'],
            'doc_model': self.env['detalle.libro.venta'],
            'data': data,
            'docs': self.env['detalle.libro.venta'].browse(data['form']['id']),
            'dosificacion' : {'razon_social': dosificacion.razon_social_empresa or 0, 'nit_empresa': dosificacion.nit_empresa or 0},
            'lines': self._detalle(data),
            'total_importe_total_a' : total_importe_total_a,
            'total_importe_ice_b' : total_importe_ice_b,
            'total_export_opera_exentas_c' : total_export_opera_exentas_c,
            'total_ventas_gravadas_d' : total_ventas_gravadas_d,
            'total_subtotal_e' : total_subtotal_e,
            'total_rebajas_f' : total_rebajas_f,
            'total_importe_base_g' : total_importe_base_g,
            'total_debito_fiscal_h' : total_debito_fiscal_h,
        }