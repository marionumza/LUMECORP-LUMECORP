# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

class LibroCompras(models.AbstractModel):
    _name = 'report.codigo_control.libro_compras'
    _description = 'Reporte de libro de compras'

    def monto_descuento(self, invoice):
        descuento = 0
        for line in invoice.invoice_line_ids:
            if invoice.metodo_descuento == 'Fijo': descuento += line.discount
            elif invoice.metodo_descuento == 'Porcentaje': descuento += ((line.quantity*line.price_unit) * (line.descuento / 100))
        return descuento

    def _detalle(self, data):
        invoices = self.env['account.invoice'].search([('type','=','in_invoice'), ('date_invoice','>=',data['form']['fecha_inicial']), ('date_invoice','<=',data['form']['fecha_final'])]
                                                      ,order='date_invoice ASC, numero_factura ASC')
        if data['form']['estados']== 'Confirmadas': invoices.filtered(lambda o: o.state in ['open'])
        if data['form']['estados']== 'Pagadas': invoices.filtered(lambda o: o.state in ['paid'])

        numero = 0
        detalle_factura = []
        dic = {}
        for invoice in invoices:
            numero += 1
            dic = {
                'nro': numero,                                                                                          # Número
                'fecha_factura': datetime.strptime(str(invoice.date_invoice), "%Y-%m-%d").strftime("%d/%m/%Y") or '0',  # Fecha de la Factura o DUI
                'nit_proveedor': invoice.nit,                                                                           # NIT Proveedor
                'razon_social': invoice.razon_social,                                                                   # Nombre o Razón Social
                'numero_factura': invoice.numero_factura,                                                               # Nro de la Factura
                'numero_dui': invoice.numero_dui,                                                                       # Nro de DUI
                'numero_autorizacion': invoice.numero_autorizacion,                                                     # Nro de Autorización
                'importe_total_a': (invoice.amount_total+self.monto_descuento(invoice)),                                # Importe Total de la Compra (A)
                'importe_no_sujeto_b': invoice.ice_iehd+invoice.exento,                                                 # Importe No Sujeto a Crédito Fiscal (B)
                'subtotal_c': (invoice.amount_total+self.monto_descuento(invoice))-(invoice.ice_iehd+invoice.exento),   # Subtotal (C = A - B)
                'rebajas_d': self.monto_descuento(invoice),                                                             # Descuentos, bonificaciones y Rebajas Otorgadas (D)
                'importe_debito_e': invoice.amount_total-(invoice.ice_iehd+invoice.exento),                             # Importe Base Para Crédito Fiscal (E = C - D)
                'credito_fiscal_f': (invoice.amount_total-(invoice.ice_iehd+invoice.exento))*0.13,                      # Crédito Fiscal F = E * 13%
                'codigo_control': invoice.codigo_control or '0',                                                        # Código de Control
                'tipo_compra': invoice.tipo_compra or '0',                                                              # Tipo de Compra
            }
            detalle_factura.append(dic)
        return detalle_factura

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))
        total_importe_total_a = total_importe_no_sujeto_b = total_subtotal_c = total_rebajas_d = total_importe_debito_e = total_credito_fiscal_f = 0
        for register in self._detalle(data):
            total_importe_total_a += register['importe_total_a']
            total_importe_no_sujeto_b += register['importe_no_sujeto_b']
            total_subtotal_c += register['subtotal_c']
            total_rebajas_d += register['rebajas_d']
            total_importe_debito_e += register['importe_debito_e']
            total_credito_fiscal_f += register['credito_fiscal_f']

        compania = self.env.user.company_id

        return {
            'doc_ids': data['form']['id'],
            'doc_model': self.env['account.invoice'],
            'data': data,
            'docs': self.env['account.invoice'].browse(data['form']['id']),
            'compania': {'razon_social_empresa': compania.partner_id.razon_social, 'nit_empresa': compania.partner_id.nit},
            'lines': self._detalle(data),
            'total_importe_total_a': total_importe_total_a,
            'total_importe_no_sujeto_b': total_importe_no_sujeto_b,
            'total_subtotal_c': total_subtotal_c,
            'total_rebajas_d': total_rebajas_d,
            'total_importe_debito_e': total_importe_debito_e,
            'total_credito_fiscal_f': total_credito_fiscal_f,
        }