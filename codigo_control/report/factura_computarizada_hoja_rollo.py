# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class FacturaComputarizadaHojaCarta(models.AbstractModel):
    _inherit = 'report.codigo_control.factura_computarizada_hoja_carta'
    _name = 'report.codigo_control.factura_computarizada_hoja_rollo'