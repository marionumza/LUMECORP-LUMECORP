# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class FacturaComputarizadaHojaCarta(models.AbstractModel):
    _name = 'report.codigo_control.factura_computarizada_hoja_carta'
    _description = 'Reporte de facturas computarizadas'

    def get_detalle(self, id_invoice):
        invoice = self.env['account.invoice'].search([('id','=',id_invoice)])
        nombre_producto = 'ail.name' if self.env.user.user_has_groups('product.group_product_variant') else 'pt.name'
        consulta = "SELECT COALESCE(pp.default_code,'S/C')AS codigo, "+nombre_producto+", ail.quantity, ail.price_unit, (ail.quantity * ail.price_unit)AS total "\
                   "FROM account_invoice ai "\
                   "JOIN account_invoice_line ail ON ail.invoice_id = ai.id "\
                   "JOIN product_product pp ON pp.id = ail.product_id "\
                   "JOIN product_template pt ON pt.id = pp.product_tmpl_id "\
                   "WHERE ai.id = "+str(invoice.id)+" "\
                   "ORDER BY ail.sequence"
        self.env.cr.execute(consulta)
        resultado_consulta = [i for i in self.env.cr.fetchall()]
        item = 0
        detalle_factura = []
        for consulta in resultado_consulta:
            item += 1
            concepto = consulta[1]
            if consulta[0] != 'S/C' and not self.env.user.user_has_groups('product.group_product_variant'): concepto = '['+consulta[0]+']'+" "+concepto
            dic = {
                'item': item,
                'codigo': consulta[0] or '',
                'concepto': concepto,
                'cantidad': consulta[2],
                'precio_unitario': consulta[3],
                'total': consulta[4],
            }
            detalle_factura.append(dic)

        return detalle_factura

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))
        res = {}
        invoices = []
        order = 0
        for invoice in data['form']['invoice_ids']:
            order +=1
            if order > 1: res[invoice] = self.get_detalle(invoice)
            else: res[invoice] = self.get_detalle(invoice)
            dato = {
                'invoice': self.env['account.invoice'].browse(invoice),
                'numero_impresion': order
            }
            invoices.append(dato)

        return {
            'doc_ids': data['form']['invoice_ids'],
            'doc_model': self.env['account.invoice'],
            'data': data,
            'docs': invoices,
            'lines': res,
            'copia': data['copia'],
        }