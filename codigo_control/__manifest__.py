# -*- coding: utf-8 -*-

{
    'name': "Código de Control",
    'summary': """
        Generación de códigos de control
    """,
    'description': """
        Módulo que permite la generación de códigos de control basado en las normativas bolivianas.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'base',
        'account',
        'account_cancel',
        'purchase'
    ],
    'data': [
        'data/ciudades_bolivia_data.xml',
        'data/dosificacion_demo_data.xml',
        'views/txt_model_view.xml',
        'views/xls_model_view.xml',
        'views/account.xml',
        'views/res_partner_view.xml',
        'views/generar_codigo_control_view.xml',
        'views/dosificacion_view.xml',
        'views/detalle_libro_venta_view.xml',
        'views/account_invoice_view.xml',
        'views/purchase_view.xml',
        'report/views/config_report.xml',
        'report/views/factura_computarizada_hoja_carta.xml',
        # 'report/views/factura_computarizada_hoja_rollo.xml',
        'report/views/libro_ventas.xml',
        'report/views/libro_compras.xml',
        'wizard/views/libro_ventas_wiz_view.xml',
        'wizard/views/libro_compras_wiz_view.xml',
        'views/webclient_templates.xml',
        'security/ir.model.access.csv',
        'security/ir_rules.xml',
        'data/retencion_data.xml',
        'query/configuraciones.sql',
    ],
    'application': False,
}