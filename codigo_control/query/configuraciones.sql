UPDATE account_journal SET update_posted=TRUE;
UPDATE res_partner SET tz = 'America/La_Paz' WHERE tz ISNULL;
UPDATE res_lang SET decimal_point='.', thousands_sep=',' WHERE code='es_BO';
UPDATE account_tax SET amount_type='division', include_base_amount=TRUE, price_include=TRUE;