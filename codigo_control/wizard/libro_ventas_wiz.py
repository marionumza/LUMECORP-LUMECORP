# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime
import time
import base64
import logging

_logger = logging.getLogger(__name__)

class ListaDosificacion(models.TransientModel):
    _name = 'lista.dosificacion'
    _description = 'Lista de dosificaciones disponibles para casos particulares'
    _rec_name = 'numero_autorizacion'

    numero_autorizacion = fields.Char(string='N° de Autorización')

class LibroVentasWiz(models.TransientModel):
    _name = 'libro.ventas.wiz'
    _description = 'Reporte de Libro de Ventas'

    @api.model
    def default_get(self, fields):
        res = super(LibroVentasWiz, self).default_get(fields)
        if 'dosificacion' in fields: res['dosificacion'] = self.get_dosificaciones()
        return res

    fecha_inicial = fields.Date(string='Desde la Fecha', required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    fecha_final = fields.Date(string='Hasta la Fecha', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    dosificacion = fields.Many2many('lista.dosificacion', 'dosificacion_libro_ventas_rel', 'dosificacion_id', 'libro_ventas_id', string='Dosificación', required=True)

    def get_dosificaciones(self):
        self.env.cr.execute("DELETE FROM lista_dosificacion;")
        self.env.cr.execute("""
        SELECT
        dlv.numero_autorizacion
        FROM detalle_libro_venta dlv
        WHERE dlv.numero_autorizacion NOTNULL
        GROUP BY dlv.numero_autorizacion
        ORDER BY dlv.numero_autorizacion DESC
        """)
        resultado = [i[0] for i in self.env.cr.fetchall()]
        numero_autorizacion = []
        orden = 0
        for numeros in resultado:
            orden += 1
            numero_autorizacion.append(self.env['lista.dosificacion'].create({'numero_autorizacion':numeros}).id)
        return numero_autorizacion

    @api.multi
    def imprimir_pdf(self):
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['fecha_inicial', 'fecha_final', 'dosificacion'])[0]
        data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
        return self.env.ref('codigo_control.report_libro_ventas_pdf').report_action(self, data=data)

    @api.multi
    def imprimir_facilito(self):
        nombre_compania = self.env.user.company_id.name
        numeros_autorizacion = [x.numero_autorizacion for x in self.env['lista.dosificacion'].search([('id', 'in', self.dosificacion.ids)])]
        numeros_autorizacion = str("('"+numeros_autorizacion[0]+"')") if len(numeros_autorizacion) == 1 else tuple(numeros_autorizacion)
        filename = 'ventas_'+datetime.strptime(str(self.fecha_inicial), "%Y-%m-%d").strftime("%m%Y")+('_'+nombre_compania if nombre_compania else '')+'.txt'
        aumentar = ""
        saltopagina = "\r\n"

        consulta = """
        SELECT 
        to_char(dlv.fecha_factura,'DD/MM/YYYY') as fecha
        ,dlv.numero_factura::INTEGER as numero_factura
        ,dlv.numero_autorizacion as autorizacion
        ,dlv.estado_factura as estado
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.nit_factura ELSE '0' END) as nit
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.razon_social_factura ELSE 'anulado' END) as razon
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.importe_total  ELSE 0 END) as importe_total
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.importe_ice ELSE 0 END) as ice
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.operaciones_excentas ELSE 0 END) as excentas
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.ventas_gravadas ELSE 0 END) as gravadas
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.sub_total ELSE 0 END) as sub_total
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.descuento ELSE 0 END) as descuento
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.importe_total_base ELSE 0 END) as importe_base
        ,(SELECT CASE WHEN dlv.estado_factura != 'A' THEN dlv.debito_fiscal ELSE 0 END) as debito
        ,dlv.codigo_control as control
        ,dlv.fecha_factura AS fecha_original
        FROM detalle_libro_venta dlv
        WHERE dlv.numero_factura NOTNULL AND dlv.numero_autorizacion NOTNULL AND nit_factura NOTNULL AND razon_social_factura NOTNULL """ + \
        "AND dlv.fecha_factura BETWEEN '" + str(self.fecha_inicial)+"' AND '"+str(self.fecha_final)+"' " \
        "AND dlv.numero_autorizacion IN " + str(numeros_autorizacion) + " " \
        " ORDER BY autorizacion,fecha_original,numero_factura"

        self.env.cr.execute(consulta)
        resultado_consulta = [i for i in self.env.cr.fetchall()]

        numero = 0
        for consulta in resultado_consulta:
            numero += 1
            aumentar = aumentar + "3" # 1. Especificación
            aumentar = aumentar + "|" + str(numero) # 2. Número
            aumentar = aumentar + "|" + str(consulta[0]) # 3. Fecha de la Factura
            aumentar = aumentar + "|" + str(consulta[1]) # 4. Número de Factura
            aumentar = aumentar + "|" + str(consulta[2]) # 5. Número de Autorización
            aumentar = aumentar + "|" + str(consulta[3]) # 6. Estado
            aumentar = aumentar + "|" + str(consulta[4]) # 7. NIT / CI Cliente
            aumentar = aumentar + "|" + str(consulta[5]) # 8. Razón Social
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[6])) # 9. Importe total de la venta
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[7])) # 10. Importe ICE/IEHD/IPJ/TASAS/OTROS NO SUJETOS AL IVA
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[8])) # 11. Exportaciones y Operaciones Exentas
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[9])) # 12. Ventas Gravadas a Tasa Cero
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[10])) # 13. Subtotal
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[11])) # 14. Descuentos, Bonificaciones y Rebajas Sujetas al IVA
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[12])) # 15. Importe Base Para Débito Fiscal
            aumentar = aumentar + "|" + str("{0:.2f}".format(consulta[13])) # 16. Debito Fiscal
            aumentar = aumentar + "|" + str(consulta[14] or '0') # 17. Código de Control
            aumentar = aumentar + saltopagina

        txt_libro_ventas = "NO EXISTE REGISTROS CON LOS FILTROS UTILIZADOS"
        if len(resultado_consulta) > 0:
            txt_libro_ventas = aumentar

        export_id = self.env['txt.extended']
        id_file = export_id.create({'txt_file': base64.b64encode(txt_libro_ventas.encode() or ' '), 'file_name': filename})

        return {
            'view_mode': 'form',
            'res_id': id_file.id,
            'res_model': 'txt.extended',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }