# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime
import time
import base64
import logging

_logger = logging.getLogger(__name__)

class LibroComprasWiz(models.TransientModel):
    _name = 'libro.compras.wiz'
    _description = 'Reporte de Libro de Compras'

    fecha_inicial = fields.Date(string='Desde la Fecha', required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    fecha_final = fields.Date(string='Hasta la Fecha', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    estados = fields.Selection([('Todas', 'Todas'), ('Confirmadas', 'Confirmadas'), ('Pagadas', 'Pagadas')], default='Todas', required=True)

    @api.multi
    def imprimir_pdf(self):
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['fecha_inicial', 'fecha_final', 'estados'])[0]
        data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
        return self.env.ref('codigo_control.report_libro_compras_pdf').report_action(self, data=data)

    def monto_descuento(self, invoice):
        descuento = 0
        for line in invoice.invoice_line_ids:
            if invoice.metodo_descuento == 'Fijo': descuento += line.discount
            elif invoice.metodo_descuento == 'Porcentaje': descuento += ((line.quantity*line.price_unit) * (line.descuento / 100))
        return descuento

    @api.multi
    def imprimir_facilito(self):
        nombre_compania = self.env.user.company_id.name
        filename = 'compras_'+datetime.strptime(str(self.fecha_inicial), "%Y-%m-%d").strftime("%m%Y")+('_'+nombre_compania if nombre_compania else '')+'.txt'
        aumentar = ""
        saltopagina = "\r\n"

        invoices = self.env['account.invoice'].search([('type','=','in_invoice'), ('date_invoice','>=',self.fecha_inicial), ('date_invoice','<=',self.fecha_final)]
                                                      ,order='date_invoice ASC, numero_factura ASC')
        if self.estados == 'Confirmadas': invoices.filtered(lambda o: o.state in ['open'])
        if self.estados == 'Pagadas': invoices.filtered(lambda o: o.state in ['paid'])

        numero = 0
        for invoice in invoices:
            numero += 1
            aumentar = aumentar + "1"                                                                                                                   # Especificación
            aumentar = aumentar + "|" + str(numero)                                                                                                     # Número
            aumentar = aumentar + "|" + str(datetime.strptime(str(invoice.date_invoice), "%Y-%m-%d").strftime("%d/%m/%Y") or '0')                       # Fecha de la Factura o DUI
            aumentar = aumentar + "|" + str(invoice.nit)                                                                                                # NIT Proveedor
            aumentar = aumentar + "|" + str(invoice.razon_social)                                                                                       # Nombre o Razón Social
            aumentar = aumentar + "|" + str(invoice.numero_factura)                                                                                     # Nro de la Factura
            aumentar = aumentar + "|" + str(invoice.numero_dui)                                                                                         # Nro de DUI
            aumentar = aumentar + "|" + str(invoice.numero_autorizacion)                                                                                # Nro de Autorización
            aumentar = aumentar + "|" + str("{0:.2f}".format(invoice.amount_total+self.monto_descuento(invoice)))                                       # Importe Total de la Compra (A)
            aumentar = aumentar + "|" + str("{0:.2f}".format(invoice.ice_iehd+invoice.exento))                                                          # Importe No Sujeto a Crédito Fiscal (B)
            aumentar = aumentar + "|" + str("{0:.2f}".format((invoice.amount_total+self.monto_descuento(invoice))-(invoice.ice_iehd+invoice.exento)))   # Subtotal (C = A - B)
            aumentar = aumentar + "|" + str("{0:.2f}".format(self.monto_descuento(invoice)))                                                            # Descuentos, bonificaciones y Rebajas Otorgadas (D)
            aumentar = aumentar + "|" + str("{0:.2f}".format(invoice.amount_total-(invoice.ice_iehd+invoice.exento)))                                   # Importe Base Para Crédito Fiscal (E = C - D)
            aumentar = aumentar + "|" + str("{0:.2f}".format((invoice.amount_total-(invoice.ice_iehd+invoice.exento))*0.13))                            # Crédito Fiscal F = E * 13%
            aumentar = aumentar + "|" + str(invoice.codigo_control or '0')                                                                                     # Código de Control
            aumentar = aumentar + "|" + str(invoice.tipo_compra or '0')                                                                                 # Tipo de Compra
            aumentar = aumentar + saltopagina

        txt_libro_compras = "NO EXISTE REGISTROS CON LOS FILTROS UTILIZADOS"
        if len(invoices) > 0:
            txt_libro_compras = aumentar

        export_id = self.env['txt.extended']
        id_file = export_id.create({'txt_file': base64.b64encode(txt_libro_compras.encode()  or ' '), 'file_name': filename})

        return {
            'view_mode': 'form',
            'res_id': id_file.id,
            'res_model': 'txt.extended',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }