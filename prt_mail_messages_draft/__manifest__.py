# -*- coding: utf-8 -*-
{
    'name': 'Mail Messages Draft.'
            ' Save Message as Draft, Edit Message Drafts, Send Message from Draft Message',
    'version': '12.0.1.0',
    'summary': """Adds draft messages support to free 'Mail Messages Easy' app""",
    'author': 'Ivan Sokolov',
    'category': 'Social',
    'license': 'GPL-3',
    'website': 'https://demo.promintek.com',
    'live_test_url': 'https://demo.promintek.com',
    'description': """
Mail Messages Drafts
""",
    'depends': ['prt_mail_messages'],
    'images': ['static/description/banner.png'],

    'data': [
        'security/ir.model.access.csv',
        'security/rules.xml',
        'views/prt_mail_draft.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
