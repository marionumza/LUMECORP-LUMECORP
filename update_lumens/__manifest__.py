# -*- coding: utf-8 -*-

{
    'name': "Update Lumens",
    'summary': """
        Módulo de Ajustes para Lumens
    """,
    'description': """
        Módulo para las customizaciones, funcionalidaes, operaciones, y requerimientos de la empresa\n
        LUMENS - LUMECORP
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'contacts',
        'sale',
        'sale_stock',
        'account',
        'sale_management',
        'point_of_sale',
        'codigo_control',
        'facturacion_sale',
    ],
    'data': [
        'views/product_pricelist_views.xml',
        'views/res_partner_view.xml',
        'views/product_views.xml',
        'views/sale_order_view.xml',
        'views/stock_picking_views.xml',
        'views/comision_venta_view.xml',
        'views/dosificacion_view.xml',
        'views/pos_order_views.xml',
        'views/pos_box.xml',
        'views/account_invoice_view.xml',
        'report/views/config_report.xml',
        'report/views/nota_venta.xml',
        'report/views/vale_entrega.xml',
        'report/views/caja_chica.xml',
        'views/webclient_templates.xml',
        'security/ir.model.access.csv',
    ],
    'application': False,
}