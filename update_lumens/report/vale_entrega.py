# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)

class ValeEntrega(models.AbstractModel):
    _name = 'report.update_lumens.vale_entrega'
    _description = 'Reporte de vales de entrega de Entrega'

    def get_detalle(self, data):
        consulta = "SELECT sm.product_uom_qty ,sm.name ,sm.product_id "\
                   "FROM stock_move sm "\
                   "JOIN stock_picking sp ON sp.id = sm.picking_id "\
                   "WHERE sp.id =" + str(data['form']['id']) + " "\
                   "ORDER BY sm.sequence"
        self.env.cr.execute(consulta)
        resultado_consulta = [i for i in self.env.cr.fetchall()]
        detalle_reporte = []
        for consulta in resultado_consulta:
            dic = {
                'cantidad': consulta[0],
                'descripcion': consulta[1],
                'imagen': self.env['product.product'].browse(consulta[2]).image_small,
            }
            detalle_reporte.append(dic)
        return detalle_reporte

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        return {
            'doc_ids': data['form']['id'],
            'doc_model': self.env['stock.picking'],
            'data': data,
            'docs': self.env['stock.picking'].browse(data['form']['id']),
            'lines': self.get_detalle(data),
        }