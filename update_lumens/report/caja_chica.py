# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)

class CajaChica(models.AbstractModel):
    _name = 'report.update_lumens.caja_chica'
    _description = 'Reporte de Caja Chica'

    def get_detalle(self, data):
        consulta = "SELECT acbsl.date, acbsl.name, acbsl.amount, acbsl.statement_id " \
                   "FROM pos_session ps " \
                   "JOIN account_bank_statement acbs ON acbs.pos_session_id = ps.id " \
                   "JOIN account_bank_statement_line acbsl ON acbsl.statement_id = acbs.id " \
                   "WHERE ps.id = " + str(data['form']['id']) + " "\
                   "ORDER BY acbsl.id ASC"
        self.env.cr.execute(consulta)
        resultado_consulta = [i for i in self.env.cr.fetchall()]
        detalle_reporte = []
        inicio = 0
        ingreso = egreso = saldo = 0

        for consulta in resultado_consulta:
            if inicio == 0:
                statement = self.env['account.bank.statement'].search([('id','=',consulta[3])])
                dic = {
                    'fecha': statement.date,
                    'descripcion': 'SALDO INICIAL',
                    'ingreso': statement.balance_start,
                    'egreso': 0,
                    'saldo': statement.balance_start,
                }
                detalle_reporte.append(dic)
                saldo = statement.balance_start
                inicio = 1

            if consulta[2] <= 0:
                ingreso = 0
                egreso = (-1 * consulta[2])
                saldo += (ingreso - egreso)
            elif consulta[2] > 0:
                ingreso = consulta[2]
                egreso = 0
                saldo += (ingreso - egreso)

            dic = {
                'fecha': consulta[0],
                'descripcion': consulta[1],
                'ingreso': ingreso,
                'egreso': egreso,
                'saldo': saldo,
            }
            detalle_reporte.append(dic)
        return detalle_reporte

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        total_ingreso = total_egreso = total_saldo = 0
        for register in self.get_detalle(data):
            total_ingreso += register['ingreso']
            total_egreso += register['egreso']
        total_saldo = total_ingreso - total_egreso

        return {
            'doc_ids': data['form']['id'],
            'doc_model': self.env['pos.session'],
            'data': data,
            'docs': self.env['pos.session'].browse(data['form']['id']),
            'lines': self.get_detalle(data),
            'total_ingreso': total_ingreso,
            'total_egreso': total_egreso,
            'total_saldo': total_saldo,
        }