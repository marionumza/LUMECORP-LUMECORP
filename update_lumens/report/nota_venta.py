# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)

class NotaVenta(models.AbstractModel):
    _name = 'report.update_lumens.nota_venta'
    _description = 'Reporte de las notas de venta'

    def get_detalle(self, data):
        consulta = "SELECT sol.product_uom_qty,uu.name,sol.name,apt.name,sol.price_unit,sol.product_id "\
                   "FROM sale_order so "\
                   "JOIN sale_order_line sol ON sol.order_id = so.id "\
                   "JOIN product_product pp ON pp.id = sol.product_id "\
                   "JOIN product_template pt ON pt.id = pp.product_tmpl_id "\
                   "JOIN uom_uom uu ON uu.id = pt.uom_id "\
                   "LEFT JOIN account_payment_term apt ON apt.id = so.payment_term_id "\
                   "WHERE so.id = " + str(data['form']['id']) + " "\
                   "ORDER BY sol.sequence"
        self.env.cr.execute(consulta)
        resultado_consulta = [i for i in self.env.cr.fetchall()]
        item = 0
        detalle_reporte = []
        for consulta in resultado_consulta:
            dic = {
                'cantidad': consulta[0],
                # 'unidad': consulta[1],
                'descripcion': consulta[2],
                # 'plazo_entrega': consulta[3],
                'precio_unitario': consulta[4],
                'total': consulta[0]*consulta[4],
                'imagen': self.env['product.product'].browse(consulta[5]).image_small,
            }
            detalle_reporte.append(dic)
        return detalle_reporte

    def get_tiempo_validez(self, data):
        sale = self.env['sale.order'].browse(data['form']['id'])
        dias = 0
        if sale.payment_term_id:
            dias = [x.days for x in sale.payment_term_id.line_ids][0]
        return (sale.date_order + timedelta(days=15) - timedelta(hours=4)).date()

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        return {
            'doc_ids': data['form']['id'],
            'doc_model': self.env['sale.order'],
            'data': data,
            'docs': self.env['sale.order'].browse(data['form']['id']),
            'tiempo_validez': self.get_tiempo_validez(data),
            'lines': self.get_detalle(data),
        }