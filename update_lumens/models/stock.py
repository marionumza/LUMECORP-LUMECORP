# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID, _
from odoo.tools.float_utils import float_compare, float_round, float_is_zero
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class StockMove(models.Model):
    _inherit = 'stock.move'

    # def _action_done(self):
    #     self.filtered(lambda move: move.state == 'draft')._action_confirm()  # MRP allows scrapping draft moves
    #     moves = self.exists().filtered(lambda x: x.state not in ('done', 'cancel'))
    #     moves_todo = self.env['stock.move']
    #     for move in moves:
    #         if move.quantity_done <= 0:
    #             if float_compare(move.product_uom_qty, 0.0, precision_rounding=move.product_uom.rounding) == 0:
    #                 move._action_cancel()

    #     for move in moves:
    #         if move.state == 'cancel' or move.quantity_done <= 0:
    #             continue
    #         if not move.picking_id:
    #             moves_todo |= move
    #         moves_todo |= move._create_extra_move()
    #     for move in moves_todo:
    #         rounding = self.env['decimal.precision'].precision_get('Product Unit of Measure')
    #         if float_compare(move.quantity_done, move.product_uom_qty, precision_digits=rounding) < 0:
    #             qty_split = move.product_uom._compute_quantity(move.product_uom_qty - move.quantity_done,
    #                                                            move.product_id.uom_id, rounding_method='HALF-UP')
    #             new_move = move._split(qty_split)
    #             for move_line in move.move_line_ids:
    #                 if move_line.product_qty and move_line.qty_done:
    #                     # FIXME: there will be an issue if the move was partially available
    #                     # By decreasing `product_qty`, we free the reservation.
    #                     # FIXME: if qty_done > product_qty, this could raise if nothing is in stock
    #                     try:
    #                         move_line.write({'product_uom_qty': move_line.qty_done})
    #                     except UserError:
    #                         pass
    #             move._unreserve_initial_demand(new_move)
    #     moves_todo.mapped('move_line_ids')._action_done()
    #     for result_package in moves_todo \
    #             .mapped('move_line_ids.result_package_id') \
    #             .filtered(lambda p: p.quant_ids and len(p.quant_ids) > 1):
    #         if len(result_package.quant_ids.mapped('location_id')) > 1:
    #             raise UserError(_(
    #                 'You cannot move the same package content more than once in the same transfer or split the same package into two location.'))
    #     picking = moves_todo and moves_todo[0].picking_id or False
    #     moves_todo.write({'state': 'done', 'date': fields.Datetime.now()})
    #     moves_todo.mapped('move_dest_ids')._action_assign()
    #     if all(move_todo.scrapped for move_todo in moves_todo):
    #         return moves_todo

    #     if picking:
    #         picking._create_backorder()
    #     self.product_price_update_before_done()
    #     # Acá empieza la herencia de stock del modulo stock_account
    #     res = moves_todo
    #     for move in res:
    #         if move._is_in() and move._is_out():
    #             raise UserError(_("The move lines are not in a consistent state: some are entering and other are leaving the company."))
    #         company_src = move.mapped('move_line_ids.location_id.company_id')
    #         company_dst = move.mapped('move_line_ids.location_dest_id.company_id')
    #         try:
    #             if company_src:
    #                 company_src.ensure_one()
    #             if company_dst:
    #                 company_dst.ensure_one()
    #         except ValueError:
    #             raise UserError(_("The move lines are not in a consistent states: they do not share the same origin or destination company."))
    #         # SE COMENTO ESTA LINEA POR NO PERMITIR LA TRANSFERENCIA ENTRE ALMACENES DE DIFERENTE COMPAÑÍA SE ANALIZARA POR QUE ESTA ESTA RESTRICCION
    #         # if company_src and company_dst and company_src.id != company_dst.id:
    #         #     raise UserError(_("The move lines are not in a consistent states: they are doing an intercompany in a single step while they should go through the intercompany transit location."))
    #         move._run_valuation()
    #     for move in res.filtered(lambda m: m.product_id.valuation == 'real_time' and (m._is_in() or m._is_out() or m._is_dropshipped() or m._is_dropshipped_returned())):
    #         move._account_entry_move()
    #     return res

class Picking(models.Model):
    _inherit = 'stock.picking'

    def imprimir_vale_entrega(self):
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['id'])[0]
        data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
        return self.env.ref('update_lumens.report_vale_entrega').report_action(self, data=data)