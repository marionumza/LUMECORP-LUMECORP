# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare

from odoo.exceptions import UserError

from odoo.addons.purchase.models.purchase import PurchaseOrder as Purchase


class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	@api.multi
	def button_cancel(self):
		for order in self:
			# cancelar notas de comrpas
			for inv in order.invoice_ids:
				if inv and inv.state not in ('cancel', 'draft'):
					raise UserError(_("No es posible cancelar esta orden de compra. Primero debe cancelar las facturas de proveedor relacionadas."))
			for pick in order.picking_ids:
				if pick.state == 'done':
					StockReturnPicking = self.env['stock.return.picking']
					default_data = StockReturnPicking.with_context(active_ids=pick.ids, active_id=pick.id).default_get(['move_dest_exists', 'original_location_id', 'product_return_moves', 'parent_location_id', 'location_id'])
					for qty in default_data['product_return_moves']:
						qty[2]['quantity'] = self.env['stock.move'].browse(int(qty[2]['move_id'])).product_qty
					return_wiz = StockReturnPicking.with_context(active_ids=pick.ids, active_id=pick.id).create(default_data)
					res = return_wiz.create_returns()
					return_pick = self.env['stock.picking'].browse(res['res_id'])
					for picking_lines in return_pick.move_lines:
						picking_lines.quantity_done = picking_lines.product_qty
					return_pick.button_validate()

					for move in pick:
						move.update({'state': 'cancel'})
					pick.update({'state': 'cancel'})
					for move in return_pick:
						move.update({'state': 'cancel'})
					return_pick.update({'state': 'cancel'})

			# for pick in order.picking_ids:
			# 	if pick.state == 'done':
			# 		pick.devolucion_picking(pick)
			# 	pick.update({'state': 'cancel'})

			order.state = 'cancel'
			# If the product is MTO, change the procure_method of the the closest move to purchase to MTS.
			# The purpose is to link the po that the user will manually generate to the existing moves's chain.
			if order.state in ('draft', 'sent', 'to approve'):
				for order_line in order.order_line:
					if order_line.move_dest_ids:
						move_dest_ids = order_line.move_dest_ids.filtered(lambda m: m.state not in ('done', 'cancel'))
						siblings_states = (move_dest_ids.mapped('move_orig_ids')).mapped('state')
						if all(state in ('done', 'cancel') for state in siblings_states):
							move_dest_ids.write({'procure_method': 'make_to_stock'})
							move_dest_ids._recompute_state()

			for pick in order.picking_ids.filtered(lambda r: r.state != 'cancel'):
				pick.action_cancel()

			order.order_line.write({'move_dest_ids':[(5,0,0)]})

		return super(PurchaseOrder, self).button_cancel()