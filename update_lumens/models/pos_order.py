# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class PosOrder(models.Model):
	_inherit = 'pos.order'

	@api.depends('invoice_id')
	def _get_invoiced_number(self):
		for x in self:
			x.update({'invoice_number' : x.invoice_id.numero_factura or ''})

	invoice_number = fields.Char(string="Factura", compute='_get_invoiced_number', store=True, readonly=True)

	@api.multi
	def refund(self):
		"""Create a copy of order  for refund order"""
		res = super(PosOrder, self).refund()
		order = self.env['pos.order'].browse(res['res_id'])
		payment_obj = self.env['pos.make.payment']
		default_data = payment_obj.with_context(active_ids=order.ids, active_id=order.id).default_get(['session_id'
			,'journal_id'
			,'amount'
			,'payment_date'])
		return_wiz = payment_obj.with_context(active_ids=order.ids, active_id=order.id).create(default_data)
		return_wiz.check()
		if self.invoice_id:
			self.invoice_id.motivo_anulacion = 'Error de emision'
			self.invoice_id.estado_factura = 'A'
		return res