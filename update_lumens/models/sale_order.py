# -*- coding: utf-8 -*-

from odoo import fields, models, api, tools, SUPERUSER_ID, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare
from odoo.addons import decimal_precision as dp
from datetime import datetime
import time
import logging
_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # state = fields.Selection([
    #     ('draft', 'Quotation'),
    #     ('consignacion', 'Consignación'),
    #     ('sent', 'Quotation Sent'),
    #     ('sale', 'Sales Order'),
    #     ('done', 'Locked'),
    #     ('cancel', 'Cancelled'),
    # ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', track_sequence=3,
    #     default='draft')

    @api.multi
    @api.onchange('user_id')
    def _tarifas(self):
        if self.user_id:
            if self.user_id.has_group('base.group_system'):
                self.pricelist_id = self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False
                return {'domain': {'pricelist_id': [('id', 'in', self.env['product.pricelist'].search([]).ids)]}}
            else:
                if self.state == 'draft':
                    self.pricelist_id = ''
                return {'domain': {'pricelist_id': [('id', 'in', self.env['product.pricelist'].search([('usuarios_ids', '=', self.user_id.id)]).ids)]}}

    @api.multi
    @api.onchange('user_id')
    def _dosificaciones(self):
        if self.user_id:
            if self.user_id.has_group('base.group_system'):
                if self.env['dosificacion'].search([('state','=','Activo')]):
                    self.dosificacion = self.env['dosificacion'].search([('state','=','Activo')],order='fecha_limite desc',limit=1)
                return {'domain': {'dosificacion': [('id', 'in', self.env['dosificacion'].search([('state','=','Activo')]).ids)]}}
            else:
                    if self.env['dosificacion'].search([('usuarios_ids', '=', self.user_id.id),('state','=','Activo')]):
                        self.dosificacion = self.env['dosificacion'].search([('usuarios_ids', '=', self.user_id.id),('state','=','Activo')],order='fecha_limite desc',limit=1)
                    return {'domain': {'dosificacion': [('id', 'in', self.env['dosificacion'].search([('usuarios_ids', '=', self.user_id.id),('state','=','Activo')]).ids)]}}

    @api.multi
    @api.onchange('pricelist_id')
    def cambio_tarifa(self):
        self.comision_ids = self.pricelist_id.comision_ids.ids

    @api.depends('state', 'order_line.invoice_status', 'order_line.invoice_lines')
    def _get_invoiced(self):
        super(SaleOrder, self)._get_invoiced()

        for order in self:
            invoice_ids = order.order_line.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            refunds = invoice_ids.search([('origin', 'like', order.name), ('company_id', '=', order.company_id.id)]).filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            invoice_ids |= refunds.filtered(lambda r: order.name in [origin.strip() for origin in r.origin.split(',')])
            refund_ids = self.env['account.invoice'].browse()
            if invoice_ids:
                for inv in invoice_ids:
                    refund_ids += refund_ids.search([('type', '=', 'out_refund'), ('origin', '=', inv.number), ('origin', '!=', False), ('journal_id', '=', inv.journal_id.id)])

            invoice_ids_num = invoice_ids + refund_ids
            x = invoice_ids_num.filtered(lambda i: i.numero_factura != False).mapped('numero_factura')
            number = ''
            if x and invoice_ids_num:
                number = ','.join(x)

            order.update({
                'invoice_number': number,
            })

    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, domain=_tarifas, help="Pricelist for current sales order.")
    dosificacion = fields.Many2one('dosificacion', string='Dosificación', domain=_dosificaciones)
    comision_ids = fields.Many2many('comision.venta', 'sale_comision_rel', 'comision_id', 'sale_id', string='Comisiones')
    invoice_number = fields.Char(string="Factura", compute='_get_invoiced', store=True, readonly=True)

    # @api.multi
    # def imprimir_nota_venta(self):
    #     data = {}
    #     data['ids'] = self.env.context.get('active_ids', [])
    #     data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
    #     data['form'] = self.read(['id'])[0]
    #     data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
    #     return self.env.ref('update_lumens.report_nota_venta').report_action(self, data=data)

    # @api.multi
    # def action_confirm(self):
    #     lista_productos = []
    #     for line in self.order_line:
    #         if line.product_id.type == 'product':
    #             precision = line.env['decimal.precision'].precision_get('Product Unit of Measure')
    #             product_qty = line.product_uom._compute_quantity(line.product_uom_qty, line.product_id.uom_id)
    #             quantity = line.product_id.with_context(warehouse=line.order_id.warehouse_id.id).qty_available
    #             if float_compare(quantity, product_qty, precision_digits=precision) == -1 and self.state != 'consignacion':
    #                 raise models.ValidationError((('Usted no puede vender %s del producto %s.\nEl Stock en el Almacén %s es de: %s') % (product_qty, line.name, line.order_id.warehouse_id.name, quantity)))
    #     #         lista_productos.append((0, 0, {
    #     #             'id_venta': self.id,
    #     #             'id_detalle_venta': line.id,
    #     #             'producto': line.product_id.id,
    #     #             'cantidad_original': line.product_uom_qty,
    #     #             'cantidad_consignacion': self.env['stock.move'].search([('sale_line_id','=',line.id)]).product_uom_qty,
    #     #             'cantidad': line.product_uom_qty,
    #     #             'cantidad_unidad': line.product_uom.id
    #     #         }))
    #     # if self.state == 'consignacion':
    #     #     return {
    #     #         'res_model': 'solicitar.consignacion',
    #     #         'view_mode': 'form',
    #     #         'view_type': 'form',
    #     #         'type': 'ir.actions.act_window',
    #     #         'context': {
    #     #             'default_id_venta': self.id,
    #     #             'default_state': self.state,
    #     #             'default_detalle': lista_productos,
    #     #         },
    #     #         'target': 'new',
    #     #     }

    #     # Consignación Simple
    #     if self.state == 'consignacion':
    #         for picking in self.picking_ids:
    #             if picking.state == 'done':
    #                 picking.devolucion_picking(picking)
    #             picking.update({'state': 'cancel'})

    #     result = super(SaleOrder, self).action_confirm()
    #     # para confirmrar los almacenes
    #     # _logger.info('\nENTREEEEEE\n\n %r \n\n', self.picking_ids)
    #     for picking in self.picking_ids:
    #         if picking.state not in ['cancel','done']:
    #             picking.action_confirm()
    #             for picking_lines in picking.move_ids_without_package:
    #                 picking_lines.quantity_done = picking_lines.product_uom_qty
    #             picking.action_assign()
    #             picking.button_validate()
    #     return result

    # @api.multi
    # def confirmar_facturar(self):
    #     lista_productos = []
    #     for line in self.order_line:
    #         if line.product_id.type == 'product':
    #             precision = line.env['decimal.precision'].precision_get('Product Unit of Measure')
    #             product_qty = line.product_uom._compute_quantity(line.product_uom_qty, line.product_id.uom_id)
    #             quantity = line.product_id.with_context(warehouse=line.order_id.warehouse_id.id).qty_available
    #             if float_compare(quantity, product_qty, precision_digits=precision) == -1 and self.state != 'consignacion':
    #                 raise models.ValidationError((('Usted no puede vender %s del producto %s.\nEl Stock en el Almacén %s es de: %s') % (product_qty, line.name, line.order_id.warehouse_id.name, quantity)))
    #     #         lista_productos.append((0, 0, {
    #     #             'id_venta': self.id,
    #     #             'id_detalle_venta': line.id,
    #     #             'producto': line.product_id.id,
    #     #             'cantidad_original': line.product_uom_qty,
    #     #             'cantidad_consignacion': self.env['stock.move'].search([('sale_line_id','=',line.id)]).product_uom_qty,
    #     #             'cantidad': line.product_uom_qty,
    #     #             'cantidad_unidad': line.product_uom.id
    #     #         }))
    #     # if self.state == 'consignacion':
    #     #     return {
    #     #         'res_model': 'solicitar.consignacion',
    #     #         'view_mode': 'form',
    #     #         'view_type': 'form',
    #     #         'type': 'ir.actions.act_window',
    #     #         'context': {
    #     #             'default_id_venta': self.id,
    #     #             'default_state': self.state,
    #     #             'default_detalle': lista_productos,
    #     #         },
    #     #         'target': 'new',
    #     #     }

    #     # Consignación simple
    #     if self.state == 'consignacion':
    #         for picking in self.picking_ids:
    #             if picking.state == 'done':
    #                 picking.devolucion_picking(picking)
    #             picking.update({'state': 'cancel'})

    #     result = super(SaleOrder, self).confirmar_facturar()
    #     _logger.info('\nENTREEEEEE\n\n %r \n\n', self.picking_ids)
    #     for picking in self.picking_ids:
    #         if picking.state not in ['cancel','done']:
    #             picking.action_confirm()
    #             for picking_lines in picking.move_ids_without_package:
    #                 picking_lines.quantity_done = picking_lines.product_uom_qty
    #             picking.action_assign()
    #             picking.button_validate()
    #     return result

    # def venta_consignacion(self):
    #     # habilitando restriccion de disponibilidad de productos en almacen
    #     for line in self.order_line:
    #         if line.product_id.type == 'product':
    #             precision = line.env['decimal.precision'].precision_get('Product Unit of Measure')
    #             product_qty = line.product_uom._compute_quantity(line.product_uom_qty, line.product_id.uom_id)
    #             quantity = line.product_id.with_context(warehouse=line.order_id.warehouse_id.id).qty_available
    #             if float_compare(quantity, product_qty, precision_digits=precision) == -1:
    #                 raise models.ValidationError((('Usted no tiene la cantidad %s del producto %s para realizar la consignación.\nEl Stock en el Almacén %s es de: %s') % (product_qty, line.name, line.order_id.warehouse_id.name, quantity)))
    #     # Consignación Simple
    #     Picking = self.env['stock.picking']
    #     Move = self.env['stock.move']

    #     new_picking = Picking.create({
    #         'partner_id': self.partner_id.id,
    #         'move_lines': [],
    #         'picking_type_id': self.warehouse_id.out_type_id.id,
    #         'state': 'draft',
    #         'origin': self.name,
    #         'location_id': self.warehouse_id.out_type_id.default_location_src_id.id,
    #         'location_dest_id': self.partner_id.property_stock_customer.id,
    #         'scheduled_date': datetime.today().strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT),
    #     })
    #     salida = new_picking.move_lines.ids

    #     for linea in self.order_line:
    #         if linea.product_uom_qty > 0:
    #             move_new = Move.create({
    #                 'name': linea.product_id.name_get()[0][1],
    #                 'product_id': linea.product_id.id,
    #                 'product_uom_qty': linea.product_uom_qty,
    #                 # 'product_qty': linea.cantidad,
    #                 'product_uom': linea.product_uom.id,
    #                 'picking_id': new_picking.id,
    #                 'state': 'draft',
    #                 'location_id': new_picking.location_id.id,
    #                 'location_dest_id': new_picking.location_dest_id.id,
    #                 'partner_id': self.partner_id.id,
    #                 'origin': self.name,
    #                 'picking_type_id': new_picking.picking_type_id.id,
    #                 'sale_line_id': self.id,
    #             })
    #             salida.append(move_new.id)
    #     new_picking.move_lines = [(6, 0, salida)]

    #     list_picking = self.picking_ids.ids
    #     list_picking.append(new_picking.id)
    #     self.picking_ids = [(6, 0, list_picking)]
    #     new_picking.action_confirm()
    #     for picking_lines in new_picking.move_ids_without_package:
    #         picking_lines.quantity_done = picking_lines.product_uom_qty
    #     new_picking.action_assign()
    #     new_picking.button_validate()
    #     self.write({'state': 'consignacion'})

    #     # # Consignación Avanzanda
    #     # lista_productos = []
    #     # for line in self.order_line:
    #     #     if line.product_id.type == 'product':
    #     #         precision = line.env['decimal.precision'].precision_get('Product Unit of Measure')
    #     #         product_qty = line.product_uom._compute_quantity(line.product_uom_qty, line.product_id.uom_id)
    #     #         quantity = line.product_id.with_context(warehouse=line.order_id.warehouse_id.id).qty_available
    #     #         if float_compare(quantity, product_qty, precision_digits=precision) == -1:
    #     #             raise models.ValidationError((('Usted no tiene la cantidad %s del producto %s para realizar la consignación.\nEl Stock en el Almacén %s es de: %s') % (product_qty, line.name, line.order_id.warehouse_id.name, quantity)))
    #     #         lista_productos.append((0, 0, {
    #     #             'id_venta': self.id,
    #     #             'id_detalle_venta': line.id,
    #     #             'producto': line.product_id.id,
    #     #             'cantidad_original': line.product_uom_qty,
    #     #             'cantidad': line.product_uom_qty,
    #     #             'cantidad_unidad': line.product_uom.id
    #     #         }))
    #     #
    #     # return {
    #     #     'res_model': 'solicitar.consignacion',
    #     #     'view_mode': 'form',
    #     #     'view_type': 'form',
    #     #     'type': 'ir.actions.act_window',
    #     #     'context': {
    #     #         'default_id_venta': self.id,
    #     #         'default_state': self.state,
    #     #         'default_detalle': lista_productos,
    #     #     },
    #     #     'target': 'new',
    #     # }

# class SaleOrderLine(models.Model):
#     _inherit = 'sale.order.line'

#     @api.onchange('product_uom_qty', 'product_uom', 'route_id')
#     def _onchange_product_id_check_availability(self):
#         if not self.product_id or not self.product_uom_qty or not self.product_uom:
#             self.product_packaging = False
#             return {}
#         if self.product_id.type == 'product':
#             precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
#             product = self.product_id.with_context(
#                 warehouse=self.order_id.warehouse_id.id,
#                 lang=self.order_id.partner_id.lang or self.env.user.lang or 'en_US'
#             )
#             product_qty = self.product_uom._compute_quantity(self.product_uom_qty, self.product_id.uom_id)
#             if float_compare(product.virtual_available, product_qty, precision_digits=precision) == -1:
#                 is_available = self._check_routing()
#                 if not is_available:
#                     message = _('You plan to sell %s %s of %s but you only have %s %s available in %s warehouse.') % \
#                               (self.product_uom_qty, self.product_uom.name, self.product_id.name,
#                                product.virtual_available, product.uom_id.name, self.order_id.warehouse_id.name)
#                     # We check if some products are available in other warehouses.
#                     if float_compare(product.virtual_available, self.product_id.virtual_available,
#                                      precision_digits=precision) == -1:
#                         message += _('\nThere are %s %s available across all warehouses.\n\n') % \
#                                    (self.product_id.virtual_available, product.uom_id.name)
#                         for warehouse in self.env['stock.warehouse'].search([]):
#                             quantity = self.product_id.with_context(warehouse=warehouse.id).virtual_available
#                             if quantity > 0:
#                                 message += "%s: %s %s\n" % (warehouse.name, quantity, self.product_id.uom_id.name)
#                     # COMENTADO PARA NO MOSTRAR MENSAJE AL NO TENER STOCK
#                     # warning_mess = {
#                     #     'title': _('Not enough inventory!'),
#                     #     'message': message
#                     # }
#                     # return {'warning': warning_mess}
#         return {}

class ComisionVenta(models.Model):
    _name = 'comision.venta'
    _description = 'Modelo para el registro de las comisiones de venta'
    _rec_name = 'name'

    name = fields.Char(string='Nombre', required=True)
    monto = fields.Float(string='Comisión(%)')

class SolicitarConsignacion(models.Model):
    _name = 'solicitar.consignacion'
    _description = 'Solicitud para consignar'

    id_venta = fields.Many2one('sale.order')
    detalle = fields.One2many('detalle.consignacion', 'solicitud_id', string='Detalle')
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('consignacion', 'Consignación'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True)

    def generar_consignacion(self):
        Picking = self.env['stock.picking']
        Move = self.env['stock.move']

        new_picking = Picking.create({
            'partner_id': self.id_venta.partner_id.id,
            'move_lines': [],
            'picking_type_id': self.id_venta.warehouse_id.out_type_id.id,
            'state': 'draft',
            'origin': self.id_venta.name,
            'location_id': self.id_venta.warehouse_id.out_type_id.default_location_src_id.id,
            'location_dest_id': self.id_venta.partner_id.property_stock_customer.id,
            'scheduled_date': datetime.today().strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT),
        })
        salida = new_picking.move_lines.ids

        for linea in self.detalle:
            if linea.cantidad > 0:
                move_new = Move.create({
                    'name': linea.producto.name_get()[0][1],
                    'product_id': linea.producto.id,
                    'product_uom_qty': linea.cantidad,
                    # 'product_qty': linea.cantidad,
                    'product_uom': linea.cantidad_unidad.id,
                    'picking_id': new_picking.id,
                    'state': 'draft',
                    'location_id': new_picking.location_id.id,
                    'location_dest_id': new_picking.location_dest_id.id,
                    'partner_id': self.id_venta.partner_id.id,
                    'origin': self.id_venta.name,
                    'picking_type_id': new_picking.picking_type_id.id,
                    'sale_line_id': linea.id_detalle_venta.id,
                })
                salida.append(move_new.id)
        new_picking.move_lines = [(6, 0, salida)]

        list_picking = self.id_venta.picking_ids.ids
        list_picking.append(new_picking.id)
        self.id_venta.picking_ids = [(6, 0, list_picking)]
        self.id_venta.write({'state':'consignacion'})

    def confirmar_venta(self):
        _logger.info('\n\n ACA\n%r',self.id_venta.picking_ids)

class DetalleConsignar(models.Model):
    _name = 'detalle.consignacion'
    _description = 'Detalle de la solicitud para consignar'

    id_venta = fields.Many2one('sale.order')
    id_detalle_venta = fields.Many2one('sale.order.line')
    solicitud_id = fields.Many2one('solicitar.consignacion', 'Solicitud')
    producto = fields.Many2one('product.product', string='Producto')
    cantidad_original = fields.Float(string='Cantidad Actual Venta', digits=dp.get_precision('Product Unit of Measure'))
    cantidad_consignacion = fields.Float(string='Cantidad Consignacion', digits=dp.get_precision('Product Unit of Measure'))
    cantidad = fields.Float(string='Cantidad', digits=dp.get_precision('Product Unit of Measure'))
    cantidad_unidad = fields.Many2one('uom.uom', string='Unidad')