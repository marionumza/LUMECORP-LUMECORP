# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.addons.account.wizard.pos_box import CashBox
import re
import time
import logging
_logger = logging.getLogger(__name__)

class CashBoxOut(CashBox):
    _inherit = 'cash.box.out'

    @api.multi
    def run(self):
        if self.tipo_pago == 'Factura':
            impuesto_compra = self.env['account.tax'].search([('company_id','=',self.env.user.company_id.id),('type_tax_use','=','purchase'),('active','=',True)],limit=1)
            values = {
                'partner_id': self.partner_id.id,
                'account_id' : self.partner_id.property_account_payable_id.id,
                'date_invoice' : self.fecha_factura,
                'nit' : self.nit,
                'razon_social' : self.razon_social,
                'numero_factura' : self.numero_factura,
                'codigo_control' : self.codigo_control,
                'numero_autorizacion' : self.numero_autorizacion,
                'tipo_factura_consumo' : self.tipo_factura,
                'reference' : '',
                'state' : 'paid',
                'origin' : self.env['pos.session'].search([('id', '=', self._context.get('active_id'))]).name or '',
                'ice_iehd' : self.ice_iehd,
                # 'metodo_descuento' : self.metodo_descuento,
                # 'descuento' : self.descuento,
                'invoice_line_ids': [(0,0,{
                        'name' : self.name,
                        'quantity' : 1,
                        'price_unit' : self.amount,
                        'invoice_line_tax_ids' : [(6,0,[impuesto_compra.id])] or None,
                        'account_id' : self.partner_id.property_account_payable_id.id or None
                    })]
                }
            self.env['account.invoice'].with_context(type='in_invoice').create(values)
        return super(CashBoxOut, self).run()

    @api.onchange('pagado', 'retencion', 'numero_factura', 'tipo_pago', 'partner_id')
    def onchage_concepto(self):
        motivo_completo = 'PAGO a '
        if self.partner_id:
            self.pagado = self.partner_id.name
            self.razon_social = self.partner_id.razon_social or ''
            self.nit = self.partner_id.nit or ''
        if self.pagado: motivo_completo += self.pagado
        if self.tipo_pago == 'Factura':
            motivo_completo += " - Fact. " + str(self.numero_factura or '0')
        if self.tipo_pago == 'Retención':
            if self.retencion:
                texto_retencion = ""
                for rentenciones in self.retencion:
                    texto_retencion += " " + rentenciones.name
                motivo_completo += " - " + texto_retencion
        self.name = motivo_completo

    @api.onchange('amount', 'tipo_factura')
    def onchage_calculos(self):
        if self.tipo_factura == 'Combustible': self.ice_iehd = self.amount * 0.3
        else: self.ice_iehd = 0

    @api.onchange('codigo_control')
    def cambio_codigo_control(self):
        if self.codigo_control:
            codigo_nuevo = self.codigo_control.replace('-', '')
            cadena = '-'.join(re.findall('.{1,2}', codigo_nuevo))
            self.codigo_control = cadena.upper()

    fecha_registro = fields.Date(string='Fecha Registro', default=lambda *a: time.strftime('%Y-%m-%d'))
    partner_id = fields.Many2one('res.partner', string='Proveedor/Empleado')
    pagado = fields.Char(string='Pagado a')
    tipo_pago = fields.Selection([('Retención', 'Retención'),('Factura', 'Factura')], string='Tipo de Pago', default='Factura')
    fecha_factura = fields.Date(string='Fecha Factura')
    tipo_factura = fields.Selection([('Normal', 'Normal'), ('Combustible', 'Combustible'), ('Servicios Básicos', 'Servicios Básicos')], string='Tipo Factura', default='Normal')
    tipo_compra = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')],
                                   string='Tipo de Compra', required=True, default='1',
                                   help=_('1 = Compras para mercado interno con destino a actividades gravadas,\n'
                                          '2 = Compras para mercado interno con destino a actividades no gravadas,\n'
                                          '3 = Compras sujetas a proporcionalidad,\n'
                                          '4 = Compras para exportaciones,\n'
                                          '5 = Compras tanto para el mercado interno como para exportaciones.'))
    numero_factura = fields.Char(string='N° de Factura', default='0')
    nit = fields.Char(string='NIT')
    razon_social = fields.Char(string='Razón Social')
    numero_autorizacion = fields.Char(string='N° de Autorización')
    codigo_control = fields.Char(string='Código de Control')
    ice_iehd = fields.Float(string='ICE/IEHD')
    metodo_descuento = fields.Selection([('Fijo', 'Fijo'), ('Porcentaje', 'Porcentaje')], string='Método Descuento', default='Fijo', required=True)
    descuento = fields.Float(string='Monto Descuento', help='Descuento aplicado de acuerdo al metodo')
    retencion = fields.Many2many('account.tax', string='Tipo de Retencion', domain=[('retencion','=',True),('active', '=', True)])

class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    def ver_cash(self):
        monto = self.amount if self.amount >= 0 else (self.amount * -1)
        line = self.env['account.invoice.line'].search([('name', '=', self.name), ('price_unit', '=', monto)])
        if line.invoice_id.origin == self.statement_id.name:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'account.invoice',
                'res_id': line.invoice_id.id,
                'views': [[False, 'form']],
                'target': 'new'
            }

class PosSession(models.Model):
    _inherit = 'pos.session'

    def imprimir_reporte(self):
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['id'])[0]
        data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
        return self.env.ref('update_lumens.report_caja_chica').report_action(self, data=data)