# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    nombre_sucursal = fields.Char(string='Nombre Sucursal', help='Información para uso en la cabecera de reportes')