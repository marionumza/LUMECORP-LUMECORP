# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import time
import logging
_logger = logging.getLogger(__name__)

class Dosificacion(models.Model):
    _inherit = 'dosificacion'

    usuarios_ids = fields.Many2many('res.users', 'dosificacion_usuarios_rel', 'usuario_id', 'dosificacion_id', string='Usuarios Permitidos')