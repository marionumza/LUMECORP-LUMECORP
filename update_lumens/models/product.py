# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID, _
import logging
_logger = logging.getLogger(__name__)

class ProductProduct(models.Model):
    _inherit = 'product.product'

    descripcion_variante = fields.Text(string='Sale Description')

    def get_product_multiline_description_sale(self):
        name = super(ProductProduct, self).get_product_multiline_description_sale()
        name_variant = self.name+' ('+(", ".join(str(dato.name) for dato in self.attribute_value_ids))+')'

        if name != name_variant:
            name = name_variant
        if self.descripcion_variante:
            name += '\n' + self.descripcion_variante
        return name
