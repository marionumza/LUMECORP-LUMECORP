
# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)

class Pricelist(models.Model):
    _inherit = 'product.pricelist'

    usuarios_ids = fields.Many2many('res.users', 'tarifas_usuarios_rel', 'usuario_id', 'tarifa_id', string='Usuarios Permitidos')
    comision_ids = fields.Many2many('comision.venta', 'tarifa_comision_rel', 'comision_id', 'sale_id', string='Comisiones')
