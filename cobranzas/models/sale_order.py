# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID, _
import logging
_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        return res

    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        _logger.info('\n\n %r \n\n',[res,self])
        if self:
            self.calcular_saldo(self)
        return res

    pagos_ids = fields.Many2many('account.payment', 'sale_pagos_rel', 'sale_id', 'pago_cliente_id', string='Pagos en Venta', compute='lista_pagos')
    pagos = fields.Float(string='Pagos')
    saldo = fields.Float(string='Saldo')

    def stat_pagos(self):
        self.calcular_saldo(self)
    def stat_saldo(self):
        self.calcular_saldo(self)
    def stat_lista_pagos(self):
        self.calcular_saldo(self)
        self.lista_pagos()

    def calcular_saldo(self, sale, condicion=False):
        pagos = 0
        saldo = 0
        estados = ['draft','cancelled']
        if sale.state in ('sale', 'done'):
            order = self.env['sale.order'].search([('name', '=', sale.name)])
            saldo = sale.amount_total
            if self.env['lista.ventas'].search([('venta_id','=',order.id)]):
                listas_ids = self.env['lista.ventas'].search([('venta_id','=',order.id)])
                if condicion: estados = ['cancelled']
                pagos_ids = self.env['account.payment'].search([('lista_ventas_ids','in',listas_ids.ids),('state', 'not in', estados)])
                if pagos_ids:
                    listas_ids = self.env['lista.ventas'].search([('id', 'in', listas_ids.ids),('state','=','Válido'),('payment_id','in',pagos_ids.ids)])
                    for registros in listas_ids:
                        pagos += registros.pago_venta
                    saldo = sale.amount_total - pagos
        self.env.cr.execute("UPDATE sale_order SET pagos="+str(pagos)+", saldo="+str(saldo)+" WHERE id="+str(sale.id))

    @api.multi
    def lista_pagos(self):
        for order in self:
            order.pagos_ids = self.env['account.payment'].search([('lista_ventas_ids','in',(self.env['lista.ventas'].search([('venta_id','=',order.id)]).ids)),('state', 'not in', ['draft','cancelled'])]).ids

    @api.multi
    def action_cancel(self):
        res = super(SaleOrder, self).action_cancel()
        listas = self.env['lista.ventas'].search([('venta_id','=',self.id)])
        for lista_venta in listas:
            if len(lista_venta.payment_id.lista_ventas_ids) == 1:
                lista_venta.payment_id.cancel()
            else:
                for lista in lista_venta:
                    lista.state = 'Anulado'
        for invoice in self.invoice_ids:
            if invoice.state == 'draft':
                invoice.action_invoice_cancel()
            elif invoice.state in ('open', 'paid'):
                invoice.estado_factura = 'A'
                invoice.motivo_anulacion = 'Cancelación de Factura'
                invoice.action_invoice_cancel()
        self.calcular_saldo(self)
        return res