# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID, _
import logging
_logger = logging.getLogger(__name__)

class Picking(models.Model):
    _inherit = 'stock.picking'

    def devolucion_picking(self, picking):
        srp = self.env['stock.return.picking']
        sp = self.env['stock.picking']
        field = ['move_dest_exists', 'product_return_moves', 'parent_location_id', 'original_location_id', 'location_id']
        res = srp.with_context({'active_id': picking.id}).default_get(field)
        vals = {
            'original_location_id': res['original_location_id'],
            'picking_id': res['picking_id'],
            'move_dest_exists': res['move_dest_exists'],
            'location_id': res['location_id'],
            'parent_location_id': res['parent_location_id'],
        }
        vals['product_return_moves'] = [i for i in res['product_return_moves']]
        srp_id = srp.create(vals)
        new_picking_id, pick_type_id = srp_id._create_returns()
        picking_id = sp.search([('id', '=', new_picking_id)])
        for picking_lines in picking_id.move_lines:
            picking_lines.quantity_done = picking_lines.product_qty
        picking_id.button_validate()
        for move in picking:
            move.update({'state': 'cancel'})
        picking.update({'state': 'cancel'})
        for move in picking_id:
            move.update({'state': 'cancel'})
        picking_id.update({'state': 'cancel'})