# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def unlink(self):
        for payment in self:
            if payment.state not in ('draft','cancelled'):
                for lineas in payment.lista_ventas_ids:
                    payment.env.cr.execute("UPDATE sale_order SET pagos="+str(lineas.venta_id.pagos-lineas.pago_venta)+", saldo="+str(lineas.venta_id.saldo+lineas.pago_venta)+" WHERE id="+str(lineas.venta_id.id))
            res = super(AccountPayment, self).unlink()
            return res

    @api.onchange('partner_type', 'partner_id', 'journal_id', 'amount')
    def onchange_cuenta_caja(self):
        self.tipo_diario = self.journal_id.type
        self.cuenta_caja = self.payment_type in ('outbound','transfer') and self.journal_id.default_debit_account_id.id or self.journal_id.default_credit_account_id.id
        if self.partner_id:
            lista = []
            if self.partner_type == 'customer':
                cantidad_pagar = self.amount
                for sale in self.env['sale.order'].search([('partner_id','=',self.partner_id.id),('state','in',['sale','done'])], order='id asc'):
                    self.env['sale.order'].calcular_saldo(sale)
                    if sale.saldo > 0:
                        if cantidad_pagar >= sale.saldo:
                            pago_venta = sale.saldo
                            cantidad_pagar -= sale.saldo
                            pago_total = True
                        else:
                            pago_venta = cantidad_pagar
                            cantidad_pagar = 0
                            pago_total = False

                        lista.append({
                            'venta_id': sale.id,
                            'total_venta': sale.amount_total,
                            'saldo_venta': sale.saldo,
                            'pago_venta': pago_venta,
                            'pago_total': pago_total,
                        })
                self.lista_ventas_ids = lista

    cuenta_caja = fields.Many2one('account.account', string='Cuenta Caja/Banco')
    metodo_pago = fields.Selection([('Pedido','Pedido'),('Crédito','Crédito'),('Anctipo','Anctipo')], string='Método Pago', default='Pedido')
    ventas_ids = fields.One2many('sale.order', 'pagos_ids')
    lista_ventas_ids = fields.One2many('lista.ventas', 'payment_id')
    lista_compras_ids = fields.One2many('lista.compras', 'payment_id')
    total_lista_ventas = fields.Float(string='Total Ventas', compute='_compute_total_ventas')
    pago_cliente_id = fields.Many2one('pago.cliente')
    numero_cheque_transfercia = fields.Char(string='N° Cheque/Transferencia')
    tipo_diario = fields.Char(string='Tipo Diario')
    numero_facturas = fields.Char(string='Facturas', help='Lista de facturas de los pagos', compute='_compute_facturas')
    numero_pedidos = fields.Char(string='Pedidos', help='Lista de pedidos', compute='_compute_facturas')

    def _get_liquidity_move_line_vals(self, amount):
        name = self.name
        if self.payment_type == 'transfer':
            name = _('Transfer to %s') % self.destination_journal_id.name
        vals = {
            'name': name,
            'account_id': self.cuenta_caja.id if self.cuenta_caja else self.payment_type in ('outbound','transfer') and self.journal_id.default_debit_account_id.id or self.journal_id.default_credit_account_id.id,
            'journal_id': self.journal_id.id,
            'currency_id': self.currency_id != self.company_id.currency_id and self.currency_id.id or False,
        }

        if self.journal_id.currency_id and self.currency_id != self.journal_id.currency_id:
            amount = self.currency_id._convert(amount, self.journal_id.currency_id, self.company_id, self.payment_date or fields.Date.today())
            debit, credit, amount_currency, dummy = self.env['account.move.line'].with_context(date=self.payment_date)._compute_amount_fields(amount, self.journal_id.currency_id, self.company_id.currency_id)
            vals.update({
                'amount_currency': amount_currency,
                'currency_id': self.journal_id.currency_id.id,
            })
        return vals

    @api.multi
    def post(self):
        total_pagos = 0
        for payment in self:
            if payment.payment_type == 'inbound':
                for registros in payment.lista_ventas_ids:
                    self.env['sale.order'].calcular_saldo(registros.venta_id, True)
                    if registros.pago_venta > registros.saldo_venta: registros.pago_venta = registros.saldo_venta
                    total_pagos += registros.pago_venta
                payment.amount = total_pagos
        rec = super(AccountPayment, self).post()
        return rec

    @api.multi
    def cancel(self):
        for rec in self:
            if rec.lista_ventas_ids:
                for venta in rec.lista_ventas_ids:
                    for invoice in venta.venta_id.invoice_ids:
                        if invoice.state == 'draft': invoice.action_invoice_cancel()
                        elif invoice.state in ('open','paid'):
                            invoice.estado_factura = 'A'
                            invoice.motivo_anulacion = 'Cancelación de Factura'
                            invoice.action_invoice_cancel()
                    for picking in venta.venta_id.picking_ids:
                        if picking.state in ('draft','confirmed','assigned'): venta.venta_id.action_cancel()
                        elif picking.state == 'done':
                            picking.devolucion_picking(picking)
                    for picking in venta.venta_id.picking_ids:
                        picking.update({'state': 'cancel'})
                    venta.venta_id.calcular_saldo(venta.venta_id)
                    venta.venta_id.update({'state': 'cancel'})
                    venta.state = 'Anulado'
            else:
                pos_order = self.env['pos.order'].search([('name','ilike',rec.communication.strip().replace(':',''))])
                if pos_order:
                    if pos_order.invoice_id.state == 'draft':
                        pos_order.invoice_id.action_invoice_cancel()
                    elif pos_order.invoice_id.state in ('open', 'paid'):
                        pos_order.invoice_id.estado_factura = 'A'
                        pos_order.invoice_id.motivo_anulacion = 'Cancelación de Pago'
                        pos_order.invoice_id.action_invoice_cancel()
                    po = self.env['pos.order']
                    pmp = self.env['pos.make.payment']
                    return_pos_id = pos_order.refund()
                    return_pos = po.search([('id','=',return_pos_id.get('res_id'))])
                    vals = {'active_id': return_pos.id}
                    pmp_id = pmp.with_context({'active_id':return_pos.id}).create(vals)
                    pmp_id.check()

            for move in rec.move_line_ids.mapped('move_id'):
                # Removimiendo validación si tiene lineas de factura
                move.line_ids.remove_move_reconcile()
                move.button_cancel()
                move.unlink()
            rec.state = 'cancelled'

    @api.multi
    def _compute_total_ventas(self):
        for payment in self:
            payment.total_lista_ventas = 0
            if payment.state not in ('draft', 'cancelled'):
                if payment.lista_ventas_ids:
                    for registros in payment.lista_ventas_ids:
                        if registros.state == 'Válido':
                            if payment.payment_type == 'inbound': payment.total_lista_ventas += registros.pago_venta
                            elif payment.payment_type == 'outbound': payment.total_lista_ventas -= registros.pago_venta
                else:
                    if payment.payment_type == 'inbound': payment.total_lista_ventas += payment.amount
                    if payment.payment_type == 'outbound': payment.total_lista_ventas -= payment.amount

    @api.multi
    def _compute_facturas(self):
        for payment in self:
            lista_facturas = ''
            lista_pedidos = ''
            if self.env['ir.module.module'].search([('name', '=', 'point_of_sale')]).state == 'installed':
                if payment.communication:
                    lista_facturas = self.env['account.invoice'].search([('origin', '=', payment.communication.strip().replace(':','')),('numero_factura','!=','')], order='numero_factura').numero_factura or ''
                    lista_pedidos = self.env['account.invoice'].search([('origin', '=', payment.communication.strip().replace(':','')),('numero_factura','!=','')], order='numero_factura').origin or ''
            for registros in payment.lista_ventas_ids:
                for x in self.env['account.invoice'].search([('origin', '=', registros.venta_id.name),('numero_factura','!=','')], order='numero_factura'):
                    if x.numero_factura != '':
                        if lista_facturas == '': lista_facturas = x.numero_factura
                        else: lista_facturas += ', '+ x.numero_factura
                    if x.origin != '':
                        if lista_pedidos == '': lista_pedidos = x.origin
                        else: lista_pedidos += ', '+ x.origin
            payment.numero_facturas = lista_facturas
            payment.numero_pedidos = lista_pedidos

class ListaVentas(models.Model):
    _name = 'lista.ventas'
    _description = 'Modelo para registrar Pago(s) de diferentes ventas de un Cliente'

    @api.onchange('pago_total')
    def onchange_pago_total(self):
        if self.pago_total:
            self.pago_venta = self.saldo_venta

    @api.onchange('pago_venta')
    def onchange_pago_venta(self):
        if self.pago_venta >= self.saldo_venta:
            self.pago_venta = self.saldo_venta
            self.pago_total = True
        else: self.pago_total = False

    @api.model
    def create(self, valores):
        if valores.get('pago_venta'):
            order = self.env['sale.order'].search([('id','=',valores.get('venta_id'))])
            self.env.cr.execute("UPDATE sale_order SET pagos="+str(order.pagos+valores.get('pago_venta'))+", saldo="+str(order.saldo-valores.get('pago_venta'))+" WHERE id="+str(valores.get('venta_id')))
        datos = super(ListaVentas, self).create(valores)
        return datos

    def write(self, valores):
        if valores.get('pago_venta') and self.venta_id:
            super(ListaVentas, self).write(valores)
        return super(ListaVentas, self).write(valores)

    @api.multi
    def unlink(self):
        self.env.cr.execute("UPDATE sale_order SET pagos="+str(self.venta_id.pagos-self.pago_venta)+", saldo="+str(self.venta_id.saldo+self.pago_venta)+" WHERE id="+str(self.venta_id.id))
        return super(ListaVentas, self).unlink()

    payment_id = fields.Many2one('account.payment', ondelete='cascade')
    venta_id = fields.Many2one('sale.order', string='Venta(s)', required=True, readonly=True)
    total_venta = fields.Float(string='Venta Bs.', readonly=True)
    saldo_venta = fields.Float(string='Saldo Bs.', readonly=True)
    pago_venta = fields.Float(string='Pago Bs.')
    pago_total = fields.Boolean(string='Pago Total')
    state = fields.Selection([('Válido','Válido'),('Anulado','Anulado')], string='Estado', default='Válido', readonly=True)

class ListaCompras(models.Model):
    _name = 'lista.compras'
    _description = 'Modelo para registrar Pago(s) de diferentes Compras de un Proveedor'

    payment_id = fields.Many2one('account.payment')
    compra_id = fields.Many2one('purchase.order', string='Compra(s)')
    total_compra = fields.Float(string='Compra Bs.', readonly=True)
    saldo_compra = fields.Float(string='Saldo Bs.', readonly=True)
    pago_compra = fields.Float(string='Pago Bs.')

# class ReturnPicking(models.TransientModel):
#     _inherit = 'stock.return.picking'