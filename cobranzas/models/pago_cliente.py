# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import time
from datetime import datetime, timedelta
import base64
import io
try:
    import xlwt
except ImportError:
    xlwt = None
import logging
_logger = logging.getLogger(__name__)

class PagoCliente(models.Model):
    _name = 'pago.cliente'
    _rec_name = 'name'
    _order = 'create_date desc'
    _description = 'Modelo para registar por usuarios los pagos de ventas.'

    @api.one
    @api.depends('detalle_pago_cliente')
    def _compute_total(self):
        self.calcular_pagos(self)

    name = fields.Char(string='Nombre Sesión')
    responsable = fields.Many2one('res.users', string='Responsable', required=True, default=lambda self: self.env.user.id)
    sucursal = fields.Many2one('res.company', string='Sucursal', default=lambda self: self.env.user.company_id)
    fecha_inicial = fields.Date(string='Fecha Inicial', default=lambda *a: time.strftime('%Y-%m-%d'))
    fecha_final = fields.Date(string='Fecha Final', default=lambda *a: time.strftime('%Y-%m-%d'))
    total_sesion = fields.Float(string='Total Bs.', compute='_compute_total', store=False)
    detalle_pago_cliente = fields.One2many('account.payment', 'pago_cliente_id')
    state = fields.Selection([('Borrador','Borrador'),('Abierto','Abierto'),('Cerrado','Cerrado')], string='Estado', default='Borrador')
    observaciones = fields.Text(string='Observaciones')

    def abrir(self):
        if not self.name:
            self.name = (self.env['ir.sequence'].next_by_code('pago.cliente.seq') or 'S/C') + ' - ' +  self.responsable.partner_id.name
        self.state = 'Abierto'

    def cerrar(self):
        self.calcular_pagos(self)
        self.state = 'Cerrado'

    def calcular_pagos(self, sesion):
        sesion.total_sesion = 0
        for pagos in sesion.detalle_pago_cliente:
            sesion.total_sesion += pagos.total_lista_ventas

    def tipo_diario(self,type):
        if type == 'bank': return 'Banco'
        elif type == 'general': return 'General'
        elif type == 'purchase': return 'Compra'
        elif type == 'sale': return 'Venta'
        elif type == 'cash': return 'Efectivo'
        elif type == 'cash': return 'Efectivo'
        else: return 'Otro'

    @api.multi
    def imprimir_pdf(self):
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['id'])[0]
        data['form']['used_context'] = dict(lang=self.env.context.get('lang', 'es_BO'))
        return self.env.ref('cobranzas.report_cobranzas_pdf').report_action(self, data=data)

    def imprimir_excel(self):
        filename = 'Reporte de Cobranzas.xls'
        workbook = xlwt.Workbook(encoding="UTF-8")
        worksheet = workbook.add_sheet('Reporte de Cobranzas')
        worksheet.set_portrait(True)  # Posicion del Papel Vertical
        worksheet.paper_size_code = 1  # Tamaño del Papel: Carta

        stylePC = xlwt.XFStyle()
        alignment = xlwt.Alignment()
        alignment.horz = xlwt.Alignment.HORZ_CENTER
        fontP = xlwt.Font()
        fontP.bold = True
        fontP.height = 200
        stylePC.font = fontP
        stylePC.num_format_str = '@'
        stylePC.alignment = alignment

        worksheet.col(0).width = 260 * 18
        worksheet.col(1).width = 260 * 8
        worksheet.col(2).width = 260 * 20
        worksheet.col(3).width = 260 * 10
        worksheet.col(4).width = 260 * 13
        worksheet.col(5).width = 260 * 10
        worksheet.col(6).width = 260 * 10

        titulo = xlwt.easyxf('font:height 400, bold True, name Arial Narrow; align: horiz center, vert center;')
        titulo2 = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz center, vert center,wrap on;borders: top medium,bottom medium')
        titulo3 = xlwt.easyxf('font:height 200, bold True, name Arial Narrow; align: horiz center, vert center')
        titulo4 = xlwt.easyxf('font:height 200, bold True, name Arial Narrow; align: horiz left, vert center')
        titulo5 = xlwt.easyxf('font:height 280, bold True, name Arial Narrow; align: horiz left, vert center')
        info = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz left, vert center;')
        info2 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz left, vert center;')
        info3 = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz left, vert top;')

        datos1 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz left, vert center, wrap on;')
        datos2 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz center, vert center, wrap on;')
        datos3 = xlwt.easyxf('font:height 160, bold False, name Arial Narrow; align: horiz right, vert center, wrap on;',num_format_str='#,##0.00')
        datos3_negrita = xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz right, vert center, wrap on;',num_format_str='#,##0.00')

        totales = xlwt.easyxf('font:height 140, bold True; align: horiz right, vert center; borders: top medium',num_format_str='#,##0.00')

        firmas = xlwt.easyxf('font:height 140, bold True; align: horiz center, vert center; borders: top medium')
        firmas2 = xlwt.easyxf('font:height 140, bold True; align: horiz left, vert center;')

        worksheet.write_merge(0,1,0,6, 'REPORTE DE COBRANZAS', titulo)
        fecha_impreso = 'Fecha Impreso: ' + str((datetime.now() - timedelta(hours = 4)).strftime('%d-%m-%Y %H:%M:%S'))
        worksheet.write_merge(2,2,0,1,fecha_impreso, info)
        cobranza = 'Número Cobranza: ' + str(self.name[:8])
        worksheet.write_merge(3,3,0,1,cobranza, xlwt.easyxf('font:height 240, bold True, name Arial Narrow; align: horiz left, vert center;'))
        responsable = 'Responsable: ' + str(self.responsable.name)
        worksheet.write_merge(4,4,0,1,responsable, info)
        sucural = 'Sucursal: ' + str(self.sucursal.partner_id.name)
        worksheet.write_merge(4,4,3,5,sucural, info)
        fecha_inicial = 'Fecha Incial: ' + str(self.fecha_inicial)
        worksheet.write_merge(5,5,0,1, fecha_inicial, info)
        fecha_final = 'Fecha Final: ' + str(self.fecha_final)
        worksheet.write_merge(5,5,3,5, fecha_final, info)
        gran_total_texto = 'Gran Total (Bs.): ' + str(self.total_sesion)
        worksheet.write_merge(6,6,0,1, gran_total_texto, xlwt.easyxf('font:height 160, bold True, name Arial Narrow; align: horiz left, vert center;'))

        worksheet.write(8,0,'Fecha Pago',titulo2)
        worksheet.write_merge(8,8,1,2,'Empresa',titulo2)
        worksheet.write(8,3,'N° Factura',titulo2)
        worksheet.write(8,4,'N° Pedido',titulo2)
        worksheet.write(8,5,'Metodo Pago',titulo2)
        worksheet.write(8,6,'Total',titulo2)

        filas = 8
        lista_pagos = {}

        for lineas in self.detalle_pago_cliente:
            filas += 1
            lineas._compute_facturas()
            worksheet.write(filas, 0, str(lineas.payment_date), datos1)                      # Fecha Pago
            worksheet.write_merge(filas,filas, 1, 2, lineas.partner_id.name, datos1)    # Empresa
            worksheet.write(filas, 3, lineas.numero_facturas, datos1)                   # N° Factura
            worksheet.write(filas, 4, lineas.numero_pedidos, datos1)                    # N° Pedido
            worksheet.write(filas, 5, self.tipo_diario(lineas.journal_id.type), datos1) # Metodo Pago
            worksheet.write(filas, 6, lineas.total_lista_ventas, datos3)                # Total
            if not lineas.journal_id.id in lista_pagos:lista_pagos[lineas.journal_id.id]={'total': lineas.total_lista_ventas, 'diario': lineas.journal_id.name, 'tipo':lineas.journal_id.type}
            else: lista_pagos[lineas.journal_id.id]['total'] += lineas.total_lista_ventas

        filas += 2
        worksheet.write(filas,1,'Cuadro resumen',info)
        forma_pago = ''
        gran_total = 0

        for pagos in lista_pagos:
            if forma_pago != lista_pagos[pagos]['tipo']:
                forma_pago = lista_pagos[pagos]['tipo']
                filas += 1
                worksheet.write_merge(filas,filas,1,2,('Total '+self.tipo_diario(lista_pagos[pagos]['tipo'])),info)
            filas += 1
            worksheet.write(filas, 2, lista_pagos[pagos]['diario'], datos1)
            worksheet.write(filas, 3, lista_pagos[pagos]['total'], datos3)
            gran_total += lista_pagos[pagos]['total']

        filas += 1
        worksheet.write(filas, 2, 'Gran Total', info)
        worksheet.write(filas, 3, gran_total, totales)

        filas += 1
        worksheet.write(filas,0,'Observaciones',info)
        worksheet.write_merge(filas,filas+1,1,7,self.observaciones,info3)
        filas += 1

        filas += 2
        worksheet.write_merge(filas,filas,0,1,'ENTREGUÉ CONFORME',firmas)
        worksheet.write_merge(filas,filas,3,4,'RESPONSABLE',firmas)
        worksheet.write_merge(filas,filas,6,7,'RECIBÍ CONFORME',firmas)
        filas += 1
        worksheet.write_merge(filas, filas, 0, 1, 'Nombre:', firmas2)
        worksheet.write_merge(filas, filas, 3, 4, 'Nombre:', firmas2)
        worksheet.write_merge(filas, filas, 6, 7, 'Nombre:', firmas2)
        filas += 1
        worksheet.write_merge(filas, filas, 0, 1, 'CI:', firmas2)
        worksheet.write_merge(filas, filas, 3, 4, 'CI:', firmas2)
        worksheet.write_merge(filas, filas, 6, 7, 'CI:', firmas2)

        fp = io.BytesIO()
        workbook.save(fp)
        export_id = self.env['xls.extended'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        res = {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'xls.extended',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }
        return res