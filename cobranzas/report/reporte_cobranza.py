# -*- coding: utf-8 -*-

from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)

class ReporteCobranzas(models.AbstractModel):
    _name = 'report.cobranzas.reporte_cobranzas'

    def get_detalle(self, data):
        PagoCliente = self.env['pago.cliente']
        pago_cliente = PagoCliente.search([('id','=',data['form']['id'])])
        detalle_reporte = []
        lista_pagos = {}

        for lineas in pago_cliente.detalle_pago_cliente:
            lineas._compute_facturas()
            dic = {
                'fecha_pago': lineas.payment_date,                  # Fecha Pago
                'empresa': lineas.partner_id.name,                  # Empresa
                'nro_factura': lineas.numero_facturas,              # N° Factura
                'nro_pedido': lineas.numero_pedidos,                # N° Pedido
                'metodo_pago': PagoCliente.tipo_diario(lineas.journal_id.type), # Metodo Pago
                'total': lineas.total_lista_ventas,                 # Total
            }
            detalle_reporte.append(dic)
            if not lineas.journal_id.id in lista_pagos:lista_pagos[lineas.journal_id.id]={'total': lineas.total_lista_ventas, 'diario': lineas.journal_id.name, 'tipo':lineas.journal_id.type}
            else: lista_pagos[lineas.journal_id.id]['total'] += lineas.total_lista_ventas

        return detalle_reporte, lista_pagos

    @api.model
    def _get_report_values(self, docids, data=None):
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))
        lineas, pagos = self.get_detalle(data)
        detalle_pagos = []
        forma_pago = ''
        gran_total = 0

        for p in pagos:
            total = 0
            if forma_pago != pagos[p]['tipo']:
                forma_pago = pagos[p]['tipo']
                titulo = True
                texto = 'Total ' + self.env['pago.cliente'].tipo_diario(pagos[p]['tipo'])
                dic = {
                    'titulo': titulo,
                    'texto': texto,
                    'total': total,
                    'gran': False,
                    'gran_total':0,
                }
                detalle_pagos.append(dic)
            titulo = False
            texto = pagos[p]['diario']
            total = pagos[p]['total']
            gran_total += pagos[p]['total']

            dic = {
                'titulo': titulo,
                'texto': texto,
                'total': total,
                'gran': False,
                'gran_total': 0,
            }
            detalle_pagos.append(dic)

        dic = {
            'titulo': False,
            'texto': '',
            'total': 0,
            'gran': True,
            'gran_total': gran_total,
        }
        detalle_pagos.append(dic)

        return {
            'doc_ids': data['form']['id'],
            'doc_model': self.env['pos.order'],
            'data': data,
            'docs': self.env['pago.cliente'].browse(data['form']['id']),
            'lines': lineas,
            'pagos': detalle_pagos,
            'fecha_impreso':str((datetime.now() - timedelta(hours = 4)).strftime('%d-%m-%Y %H:%M:%S')),
        }