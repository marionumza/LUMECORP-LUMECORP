# -*- coding: utf-8 -*-

{
    'name': "Cobranzas (Pagos de Clientes)",
    'summary': """
        Módulo para realizar los pagos de ventas realizadas por los clientes.
    """,
    'description': """
        Es una modificación al módulo de pagos, para realizar los pagos de una venta con o sin facturas.\n
        Posteriormente permite habilitar sesiones de cobros, y registrar desde esa interfaz los pagos.\n
        Se tiene pensado realizar la acción tanto desde ventas, como desde la sesión de cobranzas.\n
        Otra funcionalidad es permitir la conciliación de pagos efectuados antes de realizar la factura.
    """,
    'author': "Jose Maria Vasquez Velasco",
    'category': 'BAMAVA',
    'version': '12.0.1.0',
    'depends': [
        'sale',
        'sale_management',
        'account',
    ],
    'data': [
        'data/sequence_pago_cliente.xml',
        'views/account_payment_view.xml',
        'views/sale_order_view.xml',
        'views/pago_cliente_view.xml',
        'security/ir.model.access.csv',
        'report/views/config_report.xml',
        'report/views/reporte_cobranza.xml',
    ],
    'application': False,
}
