# -*- coding: utf-8 -*-
{
    'name': 'Hide Price Of The Product If User IS Not Logged In',
    'version': '1.0',
    'author': 'FreelancerApps',
    'category': 'Website',
    'depends': ['website_sale'],
    'summary': 'Hide Product Price If User IS Not Logged In',
    'description': """
This module hides the price if user is not logged in.
It also replaces Add to Cart button with 'Sign In To Check Price'

<Search Keyword for internal user only>
---------------------------------------
guest hide product price guest hide product guest hide price guest product hide guest product price hide guest product guest price hide guest price product hide guest price hide product guest product hide price product guest hide guest price 
""",
    'data': ["views/hide_price.xml"],
    'images': ['static/description/hide_product_price_banner.png'],
    'price': 5.99,
    'currency': 'EUR',
    'license': 'OPL-1',
    'installable': True,
    'auto_install': False,
}
